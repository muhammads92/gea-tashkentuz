<section id="welcome" class="page-section new-dark">
<div class="container">
    <div class="row">
		<div class="col-md-12 text-center black">
			<h2 class="heading"  ><span class="text-color">Profnastil</span></h2>
			<h3>Profil qoplamasi qurilishda keng qo'llaniladigan galvanizli metalldan tayyorlangan buyumdir. Gofrokartonni qo'llash maydoni juda keng</h3>
			<br />
			<br />
		</div>
	</div>
</div>
<section id="services" class="page-section">
<h3 align="center"><span class="text-color">Parofnastil turlari</span></h3>
            <div class="container">
                <div class="row">
                  <div class="container-fluid white general-section">
                <div id="mix-container">
                    <!-- Item 1 -->
                    <div class="grids col-xs-12 col-sm-2 col-md-4 mix all commercial">
                        <div class="grid">
                            <img src="<?= Yii::getAlias('@web')?>/img/sections/services/17.jpg" width="400" height="273" alt="Recent Work"
                            class="img-responsive" />
                            <div class="figcaption">
                            <!-- Image Popup-->
                            <a href="<?= Yii::getAlias('@web')?>/img/sections/services/17.jpg" data-rel="prettyPhoto[portfolio]">
         
                            </a> 
                            </div>
                            <div class="caption-block">
                                <h4>Profnastil 17 gofra</h4>
                                <p>Profnastil ning 17 mm li o`lchami ko`p joylarda qo`llaniladi</p>
                            </div>
                        </div>
                    </div> 

                     <div class="grids col-xs-12 col-sm-2 col-md-4 mix all commercial">
                        <div class="grid">
                            <img src="<?= Yii::getAlias('@web')?>/img/sections/services/22.1.jpg" width="400" height="273" alt="Recent Work"
                            class="img-responsive" />
                            <div class="figcaption">
                            <!-- Image Popup-->
                            <a href="<?= Yii::getAlias('@web')?>/img/sections/services/22.1.jpg" data-rel="prettyPhoto[portfolio]">
                                <i class="fa fa-search"></i>
                            </a> 
                            <a href="portfolio-single.html">
                                <i class="fa fa-link"></i>
                            </a></div>
                            <div class="caption-block">
                                <h4>Profnastil 22 gofra</h4>
                                <p>Profnastil 22 mm li o`lchami tom va uylarni yopishsda ishlatiladi</p>
                            </div>
                        </div>
                    </div> 
                     <div class="grids col-xs-12 col-sm-2 col-md-4 mix all commercial">
                        <div class="grid">
                            <img src="<?= Yii::getAlias('@web')?>/img/sections/services/35.jpg" width="400" height="273" alt="Recent Work"
                            class="img-responsive" />
                            <div class="figcaption">
                            <!-- Image Popup-->
                            <a href="<?= Yii::getAlias('@web')?>/img/sections/services/35.jpg" data-rel="prettyPhoto[portfolio]">
                                <i class="fa fa-search"></i>
                            </a> 
                            <a href="portfolio-single.html">
                                <i class="fa fa-link"></i>
                            </a></div>
                            <div class="caption-block">
                                <h4>Profnastil 35 gofra</h4>
                                <p>Profnasilni 35 mm li turi bu sanoat maydonlarida ko`p ishlatiladi</p>
                            </div>
                        </div>
                    </div> 
                     <div class="grids col-xs-12 col-sm-2 col-md-4 mix all commercial">
                        <div class="grid">
                            <img src="<?= Yii::getAlias('@web')?>/img/sections/services/57.jpg" width="400" height="273" alt="Recent Work"
                            class="img-responsive" />
                            <div class="figcaption">
                            <!-- Image Popup-->
                            <a href="<?= Yii::getAlias('@web')?>/img/sections/services/57.jpg" data-rel="prettyPhoto[portfolio]">
                                <i class="fa fa-search"></i>
                            </a> 
                            <a href="portfolio-single.html">
                                <i class="fa fa-link"></i>
                            </a></div>
                            <div class="caption-block">
                                <h4>Profnastil 57 gofra</h4>
                                <p>Profnastilni 57 mm li o`lchami juda qalin bo`lib buyurtmachi talabi bilan tayyorlanadi</p>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </section>
</section>
		<!-- welcome -->
		<section id="action" class="page-section tb-pad-20 bg-color">
            <div class="container">
				<div class="row">
					<div class="col-md-12 top-pad-10 white"  >
						<h5 class="heading">Metallga o'xshash choyshablar tom yopish uchun ishlatilishi mumkin. Lekin ular ko'pincha sanoat ob'ektlari, turar-joy binolari, binolar, garajlar va boshqa binolar bilan qoplanadi. Materialning o'zi juda yorqin va qimmat emas. Kompaniyaning Albatross kompaniyasi har xil ehtiyojlar uchun profilli qoplamalar taklif qilishga tayyor. Bunday uyingizda katta quvvatga muhtoj emassiz. Vertikal taxtadan tomni o'rnatish, panjaraga o'xshash printsip asosida amalga oshiriladi. Faqat profilli kvadrat quvurlar ostida etarli emas, chunki tomning burishishi bo'ladi. Bu erda umumiy tuzilishni kuchaytiradigan va chayqalishlar paydo bo'lishining oldini olish uchun maxsus trusslar kerak.</h5>
					</div>
				</div>
			</div>
        </section>
        <!-- call to action -->
        <section id="services" class="page-section">
			<div class="image-bg content-in fixed" data-background="img/sections/bg/intro.jpg"></div>
            <div class="container">
				<div class="row">
                    <div class="col-md-12 bottom-pad-30 text-center black"  >
                        <!-- Title -->
                        <h2 class="heading"> <span class="text-color">Batafsil ma`lumot</span></h2>
                    </div>
                </div>
                <div class="row">
                        <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-40" data-animation="fadeInLeft">
							<div class="box-item">
								<p class="text-center">
									<a href="img/sections/services/1.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
										<img src="img/sections/services/1.jpg" width="420" height="280" alt="" />
									</a>
								</p>
								<h4>
									<a href="services-1.html">Profnastil haqida</a>
								</h4>
								<p>Profillangan choyshab - bu hozirgi zamonda biz qilolmaydigan bir xil qurilish materialidir. Turli qurilish ishlarini bajarish uchun foydalaniladi va bizning "MUSAFFO UMID"MCHJ firmamiz sizga yordam beradi. Shunday qilib, gofrokarton turli xil polimerlar tomonidan qayta ishlangan maxsus profilli qatlamdir.Bu qatalamlarni maydonlari juda keng</p>
								
							</div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-40"  >
							<div class="box-item">
								<p class="text-center">
									<a href="img/sections/services/2.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
										<img src="img/sections/services/2.jpg" width="420" height="280" alt="" />
									</a>
								</p>
								<h4>
									<a href="services-2.html">Profnastil haqida</a>
								</h4>
								<p>Bizning "MUSAFFO UMID"MCHJ firmasi bunday qurilish materiallarini bunday sifat bilan ta'minlashga qodir. Bundan tashqari, biz kerakli ma'lumotni olish uchun ushbu materialni tanlashda sizga yordam bera olamiz, chunki vazalar , har xil o'lchamlarga ega va taxminiy, ammo aniq bo'lmagan materialdir.</p>
								
							</div>
                        </div>

                         <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-40"  >
							<div class="box-item">
								<p class="text-center">
									<a href="img/sections/services/2.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
										<img src="img/sections/services/2.jpg" width="420" height="280" alt="" />
									</a>
								</p>
								<h4>
									<a href="services-2.html">Profnastil haqida</a>
								</h4>
								<p>Yuqorida aytib o'tilganidek, profilli qatlamlar turli xil ilovalarga ega bo'lgan qurilish materialidir. U juda ko'p ijobiy fazilatlarga ega va bizning "MUSAFFO UMID"MCHJ firmani sizni uni isbotlashga tayyor. Uning imkoniyatlari cheksizdir, chunki ushbu material keng ko'lamdagi dasturlarga ega. Misol uchun, bu qurilish materiallari ko'pincha tomni yaratish</p>
								
							</div>
                        </div>
                       
                        
                        
                </div>
				
            </div>
        </section>
        <!-- Services -->
        <!-- support -->
       
       
        <!-- works -->
        <section id="buy-now" class="page-section tb-pad-10 bg-color">
            <div class="container">
				<div class="row">
					<div class="col-md-12 text-center white"  >
						<h2 class="heading">Aksessuarlar
					</div>
				</div>
			</div>
        </section>
        <!-- buy now -->
             <section id="team" class="page-section light-bg border-tb">   
            <div class="container">
				<div class="row">
                    <div class="row text-center">
                    <!-- <div class="owl-carousel navigation-1"> -->
                       
                        <div class="row">
                        <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-20">
                            <div class="team-item dark-bg">
                                <div class="image">
                                    <!-- Image -->
                                    <img src="<?= Yii::getAlias('@web')?>/img/sections/services/14mkk.jpg" alt="" title="" width="270" height="270" />
                                </div>
                                <div class="description">
                                    <!-- Name -->
                                    <h4 class="name">Profnastil aksessuarlari</h4>
                                    <!-- Designation -->
                                    <div class="role">Tiz (tirsak)</div>
                                    <!-- Text -->
                                    <p>Tirsak universaldir - drenaj tizimining elementi drenaj ko'taruvchini yo'nalishini o'zgartirish uchun mo'ljallangan</p>
                                </div>
                                
                            </div>
                        </div>
                         <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-20">
                            <div class="team-item dark-bg">
                                <div class="image">
                                    <!-- Image -->
                                    <img src="<?= Yii::getAlias('@web')?>/img/sections/services/14.jpg" alt="" title="" width="270" height="270" />
                                </div>
                                <div class="description">
                                    <!-- Name -->
                                    <h4 class="name">Profnastil aksessuarlari</h4>
                                    <!-- Designation -->
                                    <div class="role">Endowa</div>
                                    <!-- Text -->
                                    <p>Endova - tomning strukturaviy elementi, 
								ikkita patnisni joylashtirish nuqtasida tashkil etilgan ichki burchak</p>
                                </div>
                                
                            </div>
                        </div>
                         <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-20">
                            <div class="team-item dark-bg">
                                <div class="image">
                                    <!-- Image -->
                                    <img src="<?= Yii::getAlias('@web')?>/img/sections/services/3.jpg" alt="" title="" width="270" height="270" />
                                </div>
                                <div class="description">
                                    <!-- Name -->
                                    <h4 class="name">Profnastil aksessuarlari</h4>
                                    <!-- Designation -->
                                    <div class="role">Staple, kanca
                                     </div>
                                    <!-- Text -->
                                    <p>Oltoyning kancasi uzun - drenaj tizimining elementi - bu oltinning kuchli tuzilishi jihatdan ko`p sohalarda ishlatiladi</p>
                                </div>
                                
                            </div>
                           </div>
                           </div>

                           <div class="row">
                        <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-20">
                            <div class="team-item dark-bg">
                                <div class="image">
                                    <!-- Image -->
                                    <img src="<?= Yii::getAlias('@web')?>/img/sections/services/22.jpg" alt="" title="" width="270" height="270" />
                                </div>
                                <div class="description">
                                    <!-- Name -->
                                    <h4 class="name">Profnastil aksessuarlari</h4>
                                    <!-- Designation -->
                                    <div class="role">Tiz (tirsak)</div>
                                    <!-- Text -->
                                    <p>Tirsak universaldir - drenaj tizimining elementi drenaj ko'taruvchini yo'nalishini o'zgartirish uchun mo'ljallangan</p>
                                </div>
                                
                            </div>
                        </div>
                         <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-20">
                            <div class="team-item dark-bg">
                                <div class="image">
                                    <!-- Image -->
                                    <img src="<?= Yii::getAlias('@web')?>/img/sections/services/6.jpg" alt="" title="" width="270" height="270" />
                                </div>
                                <div class="description">
                                    <!-- Name -->
                                    <h4 class="name">Profnastil aksessuarlari</h4>
                                    <!-- Designation -->
                                    <div class="role">Endowa</div>
                                    <!-- Text -->
                                    <p>Endova - tomning strukturaviy elementi, 
								ikkita patnisni joylashtirish nuqtasida tashkil etilgan ichki burchak</p>
                                </div>
                                
                            </div>
                        </div>
                         <div class="col-sm-4 col-md-4 col-xs-12 bottom-pad-20">
                            <div class="team-item dark-bg">
                                <div class="image">
                                    <!-- Image -->
                                    <img src="<?= Yii::getAlias('@web')?>/img/sections/services/5.jpg" alt="" title="" width="270" height="270" />
                                </div>
                                <div class="description">
                                    <!-- Name -->
                                    <h4 class="name">Profnastil aksessuarlari</h4>
                                    <!-- Designation -->
                                    <div class="role">Staple, kanca
                                     </div>
                                    <!-- Text -->
                                    <p>Oltoyning kancasi uzun - drenaj tizimining elementi - bu oltinning kuchli tuzilishi jihatdan ko`p sohalarda ishlatiladi</p>
                                </div>
                                
                            </div>
                           </div>
                           </div>
                </div>
                </div>
                </div>
                </section>
				
           
      