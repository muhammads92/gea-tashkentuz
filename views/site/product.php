<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\DetailView;
use app\models\Category;

/* @var $resetPasswordForm \app\models\forms\ResetPasswordForm */

// $this->title = $model->title_uz;
// $category = Category::findOne($model->category_id);
?>
<!-- Sticky Navbar -->
<div class="page-header page-title-left page-title-pattern">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="title">
          <?php if (\Yii::$app->language =='uz'): ?>
            Бизнинг махсулотлар
          <?php elseif (\Yii::$app->language =='ru'): ?>
            Наши продукты
          <?php endif; ?>
        </h1>
        <h5>
          <?php if (\Yii::$app->language =='uz'): ?>
            Gea Farm Technologies Узбекистандаги расмий дилер
          <?php elseif (\Yii::$app->language =='ru'): ?>
            Gea Farm Technologies Официальный дилер в Узбекистане
          <?php endif; ?>
        </h5>
      </div>
    </div>
  </div>
</div>
<!-- page-header -->
<section id="services" class="page-section service-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-9 col-md-push-3">
        <div class="row">
          <div class="col-md-12 content-block">
            <div class="text-center">
              <div class="owl-carousel navigation-4" data-pagination="false" data-items="1"
              data-singleitem="true" data-autoplay="true" data-navigation="true">
              <a href="<?= Yii::getAlias('@web')?>/img/sections/slider/slide1.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                <img src="<?= Yii::getAlias('@web')?>/img/sections/slider/slide1.jpg" width="1000" height="500" alt="" />
              </a>
              <a href="<?= Yii::getAlias('@web')?>/img/sections/slider/slide2.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                <img src="<?= Yii::getAlias('@web')?>/img/sections/slider/slide2.jpg" width="1000" height="500" alt="" />
              </a>
            </a>
            <a href="<?= Yii::getAlias('@web')?>/img/sections/slider/slide3.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
              <img src="<?= Yii::getAlias('@web')?>/img/sections/slider/slide3.jpg" width="1000" height="500" alt="" />
            </a>
          </div>
        </div>
        <p>
          <?php if (\Yii::$app->language == 'ru'): ?>
            Профессиональное оборудование животноводческих комплексов. Это его основное преимущество, и это придает ему большую гибкость. Профессиональное оборудование животноводческих комплексов. Это его основное преимущество, и это придает ему большую гибкость.
          <?php elseif (\Yii::$app->language == 'uz'): ?>
            Чорвачилик комплексларини профессионал жиҳозлаш. Бу унинг асосий устунлиги ҳисобланади ва бу уни кенг қамровли қуллаш имкониятини беради. Чорвачилик комплексларини профессионал жиҳозлаш. Бу унинг асосий устунлиги ҳисобланади ва бу уни кенг қамровли қуллаш имкониятини беради.
          <?php endif; ?>
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 content-block opacity">
        <img src="<?= Yii::getAlias('@web')?>/img/sections/slider/slide1.jpg" width="1000" height="500" alt="" />
      </div>
      <div class="col-md-6 content-block opacity">
        <img src="<?= Yii::getAlias('@web')?>/img/sections/slider/slide2.jpg" width="1000" height="500" alt="" />
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 content-block opacity">
        <h5>GEA FARM TECHNOLOGIES</h5>
        <p>Концерн GEA является одним из крупнейших поставщиков производственных технологий для глобальной молочной и пищевой промышленности, а также для широкого спектра других отраслей промышленности.</p>
        <p>Наши усилия нацелены на разработку лидирующих в мире инженерно-технических решений и компонентов для сложных производственных процессов.</p>
        <p> Деятельность компании GEA в области разработки и производства продукции и оказания услуг по поставке технологических решений ведется по двум направлениям « Оборудование» и « Решение».  Объединение деятельности на два направления, практически равных по эффективности и работающих по собственным успешным реализуемым бизнес моделям, гарантирует получение операционного синергетического эффекта.</p>
      </div>
    </div>
    <hr class="top-margin-0" />
    <div class="row">
      <div class="col-md-6 service-list content-block">
        <h4>
          <?php if (\Yii::$app->language == 'uz'): ?>
            Нима учун бизни танлашади?
          <?php elseif (\Yii::$app->language == 'ru'): ?>
              Почему выбирают нас
            <?php endif; ?>
          </h4>
          <ul>
            <li>
              <i class="icon-alarm3 text-color"></i>
              <p>
                <?php if (\Yii::$app->language == 'uz'): ?>
                  Биз дам олиш кунларисиз ишлаймиз
                <?php elseif (\Yii::$app->language == 'ru'): ?>
                    Мы работаем без выходных)
                  <?php endif; ?>
                </p>
              </li>
              <li>
                <i class="icon-shield2 text-color"></i>
                <p>
                  <?php if (\Yii::$app->language == 'uz'): ?>
                    Узоқ йиллик тажриба егамиз
                  <?php elseif (\Yii::$app->language == 'ru'): ?>
                    У нас многолетний опыт
                  <?php endif; ?>
                </p>
              </li>
            </div>
          </div>
        </div>
        <div class="sidebar col-sm-12 col-md-3 col-md-pull-9">
          <div class="widget list-border">
            <div class="widget-title">
              <h3 class="title">
                <?php if (\Yii::$app->language =='uz'): ?>
                  Бизнинг махсулотлар
                <?php elseif (\Yii::$app->language =='ru'): ?>
                  Наши продукты
                <?php endif; ?>
              </h3>
            </div>
            <div id="MainMenu1">
              <div class="list-group panel">
                <div class="collapse in" id="demo">
                  <a href="" class="list-group-item">
                    <?php if (\Yii::$app->language == 'uz'): ?>
                      Ичимликлар
                    <?php elseif (\Yii::$app->language == 'ru'): ?>
                      Напитки
                    <?php endif; ?>
                  </a>
                  <a href="" class="list-group-item">
                    <?php if (\Yii::$app->language == 'uz'): ?>
                      КимЁвий моддалар
                    <?php elseif (\Yii::$app->language == 'ru'): ?>
                      Химические препараты
                    <?php endif; ?>
                  </a>
                  <a href="" class="list-group-item">
                    <?php if (\Yii::$app->language == 'uz'): ?>
                      Сут фермаси
                    <?php elseif (\Yii::$app->language == 'ru'): ?>
                      Молочное животноводство
                    <?php endif; ?>
                  </a>
                  <a href="" class="list-group-item">
                    <?php if (\Yii::$app->language == 'uz'): ?>
                      Сут махсулотларини кайта ишлаш
                    <?php elseif (\Yii::$app->language == 'ru'): ?>
                      Переработка молочных продуктов
                    <?php endif; ?>
                  </a>
                  <a href="" class="list-group-item active">
                    <?php if (\Yii::$app->language == 'uz'): ?>
                      Озик-овкат махсулотлари
                    <?php elseif (\Yii::$app->language == 'ru'): ?>
                      Продукты питания
                    <?php endif; ?>
                  </a>
                  <a href="" class="list-group-item">
                    <?php if (\Yii::$app->language == 'uz'): ?>
                      Фармацевтика махсулотлари
                    <?php elseif (\Yii::$app->language == 'ru'): ?>
                      Фармацевтические средства
                    <?php endif; ?>
                  </a>
                </div>
              </div>
            </div>
            <!-- category-list -->
          </div>
          <div class="widget">
            <div class="widget-title">
              <h3 class="title">
                <?php if (\Yii::$app->language == "uz"): ?>
                  Юклашга
                <?php elseif (\Yii::$app->language == "ru"): ?>
                  Для скачивания
                <?php endif; ?>
              </h3>
            </div>
            <div id="MainMenu2">
              <div class="list-group list-icons panel">
                <div class="collapse in" id="demo1">
                  <a href="#" class="list-group-item pdf">Expo Ferma-2018
                    <i class="fa fa-file-pdf-o"></i>
                  </a>
                  <a href="#" class="list-group-item pdf">Прайслист
                    <i class="fa fa-file-pdf-o"></i>
                  </a>
                </div>
              </div>
              <!-- category-list -->
            </div>
            <div class="widget">
              <div class="widget-title">
                <h3 class="title">Галерея</h3>
              </div>
              <div class="owl-carousel navigation-4" data-pagination="false" data-items="1" data-autoplay="true" data-navigation="true">
                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/1.png" width="270" height="270" alt="" />
                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/2.png" width="270" height="270" alt="" />
                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/3.png" width="270" height="270" alt="" />
                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/4.png" width="270" height="270" alt="" />
                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/5.png" width="270" height="270" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Services -->
