<!-- Sticky Navbar -->
<div class="slider rs-slider-full border-bottom">
    <div class="tp-banner-container"> 
        <div class="tp-banner-full">
            <ul>
                <!-- Intro Slide -->
                <li data-transition="fade" data-slotamount="6" data-masterspeed="1500" data-delay="7000"
                    data-title="Intro Slide">
                    <img src="img/sections/slider/slide5.jpg" alt="" data-bgposition="left center" data-kenburns="on"
                         data-duration="16000" data-ease="Linear.easeNone" data-bgfit="115" data-bgfitend="115"
                         data-bgpositionend="right bottom" />
                    <div class="elements">
                        <h2 class="tp-caption tp-resizeme customin customout lft skewtotop title bold" data-x="right" data-y="201"
                            data-speed="800" data-start="800"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn">
                            <strong>Хўш келибсиз профнастил оламига</strong>
                        </h2>
                        <div class="tp-caption tp-resizeme customin customout lft skewtoright description " data-x="right"
                             data-y="290" data-speed="800" data-start="1200"
                             data-customin="x:-50;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.1;scaleY:0.1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn"
                             style="max-width: 800px; text-align:right">
                            <p>Proin a velit aliquam vitae malesuada rutrum. Aenean ullamcorper placerat porttitor velit
                                aliquam vitae. Aliquam a augue suscipit, bibendum luctus neque laoreet rhoncus ipsum,
                                ullamcorper</p>
                        </div>
                        <p class="tp-caption tp-resizeme lft customin customout  text-right" data-x="right" data-y="350"
                           data-speed="800"
                           data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                           data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                           data-start="1600" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn">
                            <a href="#" class="btn btn-default">Read more</a> 
                            <a href="#" class="btn btn-default">Download now</a></p>
                    </div>
                </li>
                <!-- Slide Ends -->
                <!-- Features Slide -->
                <li data-transition="slideup" data-slotamount="6" data-masterspeed="1500" data-saveperformance="off"
                    data-delay="7000" data-title="Features Slide">
                    <img src="img/sections/slider/slide4.jpg" alt="" data-bgposition="left" data-kenburns="on"
                         data-duration="16000" data-ease="Power1.easeOut" data-bgfit="115" data-bgfitend="115"
                         data-bgpositionend="right center" />
                    <div class="elements white">
                        <h2 class="tp-caption tp-resizeme sft skewtotop title bold customin customout" data-hoffset="0"
                            data-voffset="0" data-x="0" data-y="175"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="800" data-start="800" data-startslide="1" data-easing="Power4.easeOut"
                            data-endspeed="500" data-endeasing="Power4.easeIn">Used Latest frameworks</h2>
                        <div class="tp-caption tp-resizeme lfr skewtoright description text-left " data-x="0" data-y="275"
                             data-speed="800" data-start="1200" data-easing="Power4.easeOut" data-endspeed="500"
                             data-endeasing="Power1.easeIn" style="max-width: 600px">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do usmod tempor incididunt ut
                                labore et dolore magna aliqua ut enim ad minim veniam dolore magna aliqua ut enim ad minim
                                veniam.</p>
                        </div>
                    </div>
                </li>
                <!-- Feature Slide Ends -->
                <!-- Video Slide -->
                <li data-transition="slideleft" data-slotamount="6" data-saveperformance="off" data-masterspeed="1500"
                    data-delay="7000" class="slid2" data-title="Video Slide">
                    <img src="img/sections/slider/slide3.jpg" alt="" data-bgposition="left center" data-kenburns="on"
                         data-duration="16000" data-ease="Linear.easeNone" data-bgfit="115" data-bgfitend="115"
                         data-bgpositionend="right bottom" />
                    <div class="elements white">
                        <h2 class="tp-caption tp-resizeme sft skewtotop title bold" data-x="15" data-y="221" data-speed="800"
                            data-start="800"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-easing="Power4.easeOut" data-endspeed="400" data-endeasing="Power1.easeIn">Stunning Video</h2>
                        <div class="tp-caption tp-resizeme lfl skewtoleft description col-xs-5 " data-x="0" data-y="309"
                             data-speed="800" data-start="1200" data-easing="Power4.easeOut" data-endspeed="400"
                             data-endeasing="Power1.easeIn" style="max-width: 520px;">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do usmod tempor incididunt ut
                                labore et dolore magna aliqua ut enim ad minim veniam dolore magna aliqua ut enim ad minim
                                veniam.</p>
                        </div>
                        <a href="#" class="btn btn-default tp-caption tp-resizeme customin " data-x="15" data-y="402"
                           data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                           data-speed="800" data-start="1600" data-easing="Power3.easeInOut" data-endspeed="300"
                           style="z-index: 5">Read more</a>
                        <div class="caption customin customout tp-resizeme" data-x="right" data-hoffset="0" data-y="180"
                             data-voffset="0"
                             data-customin="x:50;y:220;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="800" data-start="1800" data-startslide="1" data-easing="Power4.easeOut"
                             data-endspeed="500" data-endeasing="Power4.easeIn" data-autoplay="false"
                             data-autoplayonlyfirsttime="false">
                            <iframe src="http://player.vimeo.com/video/75260457?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff"
                                    width="480" height="275" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </li>
                <!-- Slide Ends -->
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</div>
<!-- slider -->


<section id="features" class="page-section slider-block1" data-animation="fadeInUp">
    <div class="container">
        <div class="row special-feature">
            <!-- Special Feature Box 1 -->
            <div class="col-md-3">
                <div class="s-feature-box text-center">
                    <div class="mask-top">
                        <!-- Icon -->
                        <i class="icon-magic-wand"></i> 
                        <!-- Title -->
                        <h4>Тезкор</h4>
                        <!-- Text -->
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div>
                </div>
            </div>
            <!-- Special Feature Box 2 -->
            <div class="col-md-3">
                <div class="s-feature-box text-center">
                    <div class="mask-top">
                        <!-- Icon -->
                        <i class="icon-texture"></i> 
                        <!-- Title -->
                        <h4>Юқори технология</h4>
                        <!-- Text -->
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div>
                </div>
            </div>
            <!-- Special Feature Box 3 -->
            <div class="col-md-3">
                <div class="s-feature-box text-center">
                    <div class="mask-top">
                        <!-- Icon -->
                        <i class="icon-tree3"></i> 
                        <!-- Title -->
                        <h4>Хавфсиз</h4>
                        <!-- Text -->
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div>
                </div>
            </div>
            <!-- Special Feature Box 3 -->
            <div class="col-md-3">
                <div class="s-feature-box text-center">
                    <div class="mask-top">
                        <!-- Icon -->
                        <i class="icon-group-outline"></i> 
                        <!-- Title -->
                        <h4>Ҳамён Боп</h4>
                        <!-- TextҚҚ -->
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- features -->