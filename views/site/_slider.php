<section class="slider border-bottom line tp-banner-fullscreen-container">
  <div class="tp-banner">
    <ul>
      <li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">
        <div class="container">
          <h2 class="tp-caption tp-resizeme lft skewtotop title bold white" data-x="02" data-y="181" data-speed="1000"
          data-start="1700" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn">
          <strong>Gea Farm Technologies</strong>
        </h2>
        <h2 class="tp-caption tp-resizeme lft skewtotop title bold white" data-x="02" data-y="241" data-speed="1200"
        data-start="1900" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn">
        <strong>
          <?php if (\Yii::$app->language == 'uz'): ?>
            Узбекистондаги расмий дилери
          <?php elseif (\Yii::$app->language == 'ru'): ?>
            Официальный дилер в Узбекистане
          <?php endif; ?>
        </strong>
      </h2>
    </div>
    <img src="<?= Yii::getAlias('@web')?>/img/sections/slider/slide3.png" alt="" data-bgfit="cover" data-bgposition="center top"
    data-bgrepeat="no-repeat" />
  </li>
  <li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">
    <div class="container">
      <h2 class="tp-caption tp-resizeme lft skewtotop title bold white" data-x="02" data-y="181" data-speed="1000"
      data-start="1700" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn">
      <strong>Gea Farm Technologies</strong>
    </h2>
    <h2 class="tp-caption tp-resizeme lft skewtotop title bold white" data-x="02" data-y="241" data-speed="1200"
    data-start="1900" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn">
    <strong>
      <?php if (\Yii::$app->language == 'uz'): ?>
        Узбекистондаги расмий дилери
      <?php elseif (\Yii::$app->language == 'ru'): ?>
        Официальный дилер в Узбекистане
      <?php endif; ?>
    </strong>
  </h2>
</div>
<img src="<?= Yii::getAlias('@web')?>/img/sections/slider/slide3.png" alt="" data-bgfit="cover" data-bgposition="center top"
data-bgrepeat="no-repeat" />
</li>
</ul>
<div class="tp-bannertimer"></div>
</div>
</section>


<section id="features" class="page-section bottom-pad-0 transparent slider-block" data-animation="fadeInUp">
  <div class="container">
    <div class="row special-feature">
      <div class="col-md-3" data-animation="fadeInLeft">
        <div class="s-feature-box text-center">
          <div class="mask-top">
            <i class="icon-magic-wand"></i>
            <h4>
              <?php if (\Yii::$app->language == 'uz'): ?>
                Ферма
              <?php elseif (\Yii::$app->language == 'ru'): ?>
                Фермы
              <?php endif; ?>
            </h4>
          </div>
          <div class="mask-bottom">
            <i class="icon-magic-wand"></i>
            <?php if (\Yii::$app->language == 'uz'): ?>
              <p>GEA Farm Technologies компаниясидан фермалар</p>
            <?php elseif (\Yii::$app->language == 'ru'): ?>
              <p>Фермы от компании GEA Farm Technologies.</p>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="col-md-3" data-animation="fadeInUp">
        <div class="s-feature-box text-center">
          <div class="mask-top">
            <i class="icon-texture"></i>
            <h4>
              <?php if (\Yii::$app->language == 'uz'): ?>
                Сутни кайта ишлаш
              <?php elseif (\Yii::$app->language == 'ru'): ?>
                Переработка молока
              <?php endif; ?>

            </h4>
          </div>
          <div class="mask-bottom">
            <i class="icon-texture"></i>
            <p>
              <?php if (\Yii::$app->language == 'uz'): ?>
                "Ок олтинни" кайта ишлаш сохасида мутахассислар
              <?php elseif (\Yii::$app->language == 'ru'): ?>
                Специалисты в области переработки "белого золота"
              <?php endif; ?>
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-3" data-animation="fadeInRight">
        <div class="s-feature-box text-center">
          <div class="mask-top">
            <i class="icon-tree3"></i>
            <h4>
              <?php if (\Yii::$app->language == 'uz'): ?>
                Озик-овкат саноати
              <?php elseif (\Yii::$app->language == 'ru'): ?>
                Пищ.промышленность
              <?php endif; ?>
            </h4>
          </div>
          <div class="mask-bottom">
            <i class="icon-tree3"></i>
            <p>
              <?php if (\Yii::$app->language == 'uz'): ?>
                GEA компанияси энг яхши ечимларни такдим этади
              <?php elseif (\Yii::$app->language == 'ru'): ?>
                Компания GEA предоставляет оптимальные решения для
              <?php endif; ?>
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-3" data-animation="fadeInUp">
        <div class="s-feature-box text-center">
          <div class="mask-top">
            <i class="icon-group-outline"></i>
            <h4>
              <?php if (\Yii::$app->language == 'uz'): ?>
                Сотишдан кейинги хизмат
              <?php elseif (\Yii::$app->language == 'ru'): ?>
                Сервисное обслуживание
              <?php endif; ?>
            </h4>

          </div>
          <div class="mask-bottom">
            <i class=" icon-group-outline"></i>
            <p>
              <?php if (\Yii::$app->language == 'uz'): ?>
                Одатда, GEA компанияси ускуналари...
              <?php elseif (\Yii::$app->language == 'ru'): ?>
                Как правило, оборудование компании GEA, будь то...
              <?php endif; ?>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
