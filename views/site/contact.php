<?php
error_reporting(E_ALL);
ini_set('display_errors', 'on');

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'contact-form';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Sticky Navbar -->
<div class="page-header page-title-left page-title-pattern">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="title">
          <?php if (\Yii::$app->language == 'uz') echo 'Богланиш' ?>
            <?php if (\Yii::$app->language == 'ru') echo 'Контакты' ?>
            </h1>
            <ul class="breadcrumb">
              <li>
                <a href="/">
                  <?php if (\Yii::$app->language == 'uz') echo 'Бош сахифа' ?>
                    <?php if (\Yii::$app->language == 'ru') echo 'Главная' ?>
                    </a>
                  </li>
                  <li class="active">
                    <?php if (\Yii::$app->language == 'uz') echo 'Богланиш' ?>
                      <?php if (\Yii::$app->language == 'ru') echo 'Контакты' ?>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- page-header -->
            <section id="contact-us" class="page-section">
              <div class="container">
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="row">
                      <div class="col-sm-12 col-md-12">
                        <h5 class="title">
                          <i class="icon-address text-color"></i>
                          <?php if (\Yii::$app->language == 'uz') echo 'Бизнинг Манзилимиз' ?>
                            <?php if (\Yii::$app->language == 'ru') echo 'Наш адрес' ?>
                            </h5>
                            <address>
                              <?php if (\Yii::$app->language == 'uz') echo 'Тошкент ш.,<br/>Чилонзор туман,<br/>Қатортол кўч., 1А /2 уй' ?>
                                <?php if (\Yii::$app->language == 'ru') echo 'г. Ташкент, <br/>Чиланзарский район, <br/>улица Катартал , д. 1А/2' ?>
                                </address>
                              </div>
                               <br><br>
                               <br><br>
                               <br><br>
                              <div class="col-sm-12 col-md-12">
                                <h5 class="title">
                                  <i class="icon-contacts text-color"></i>
                                  <?php if (\Yii::$app->language == 'uz') echo 'Бизнинг телефон' ?>
                                    <?php if (\Yii::$app->language == 'ru') echo 'Наш телефон' ?>
                                </h5>
                                  <div>+998(71) 276-96-91</div>
                                  <div>+998(97) 773-09-99</div>
                                  <div><a href="mailto:gea.tashkent@gmail.com">gea.tashkent@gmail.com</a></div>
                                </div>
                              </div>
                              <hr />
                              </div>
                            <div class="col-md-6 col-md-6">
                              <h3 class="title">
                                <?php if (\Yii::$app->language == 'uz') echo 'Богланиш' ?>
                                  <?php if (\Yii::$app->language == 'ru') echo 'Контакты' ?>
                              </h3>
                              <p class="form-message"></p>
                              <div class="contact-form">
                                <!-- Form Begins -->
                                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                                <?=$form->errorSummary($model); ?>

                                <form role="form" name="contact" id="contact" method="post" action="php/contact.php">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <!-- Field 1 -->

                                      <div class="input-text form-group">
                                        <?= $form->field($model, 'full_name')->textInput()->label('Исм ') ?>
                                        <!--  <input type="text" name="contact_name" class="input-name form-control"
                                        placeholder="Full Name" /> -->
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <!-- Field 2 -->
                                      <div class="input-email form-group">
                                        <?= $form->field($model, 'email')->input('email')->label('Почта') ?>
                                        <!-- <input type="email" name="contact_email" class="input-email form-control"
                                        placeholder="Email" /> -->
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Field 3 -->
                                  <div class="input-email form-group">
                                    <?= $form->field($model, 'phone')->label('Тел номер') ?>
                                    <!-- <input type="text" name="contact_phone" class="input-phone form-control" placeholder="Phone" /> -->
                                  </div>
                                  <!-- Field 4 -->
                                  <div class="textarea-message form-group">
                                    <?= $form->field($model, 'message')->textarea()->label('Хабар'); ?>
                                    <!-- <textarea name="contact_message" class="textarea-message form-control" placeholder="Message"
                                    rows="6"></textarea>
                                  --> </div>


                                  <div>

                                    <?= Html::submitButton('Юбориш', ['class' => 'btn btn-success','name' => 'contact-button']) ?></div>

                                    <!-- Button -->
                                  </form>


                                  <?php ActiveForm::end(); ?>


                                  <!-- Form Ends -->
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>
                        <!-- page-section -->
                        <section id="map" style="max-height:400px">
                        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Af24028e258092ac31a5916284984c86836d463b62ad8e35376317fd5462ef37d&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>
                      </section>
                      <!-- map -->
