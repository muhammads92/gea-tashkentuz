
<!-- page-header -->
<section id="who-we-are" class="page-section border-tb">
  <div class="container who-we-are">
    <div class="section-title text-left">
      <!-- Title -->
      <h2 class="title" style="text-align: center;">
        <?php if (\Yii::$app->language == 'uz'): ?>
          Биз ҳақимизда
        <?php elseif (\Yii::$app->language == 'ru'): ?>
          О нас
        <?php endif; ?>
      </h2>
    </div>
    <div class="row">
      <div class="col-md-8">
        <?php if (\Yii::$app->language=='ru'): ?>
          <p>
            Наша фирма занимается проектированием нового или модернизацией имеющегося доильного оборудования по переработке молока. Специалисты нашей организации могут разработать индивидуальную концепцию для Вашего бизнеса, включая все функциональные особенности и различные потребности. Наша организация поддерживает Вас на всех стадиях оптимизации старой и возведения новой молочной фермы. Вы можете положиться на нашу проектировочную команду-многолетний опыт в проектировании и несколько удачно завершенных проектов для крупного рогатого скота. Вы получите наилучшую поддержку! Наша организация оказывает услуги по профессиональному и высококачественному проектированию молочных заводов и доильных залов от нуля по европейским стандартам. Наши специалисты проектируют молочную ферму в соответствии с индивидуальными потребностями клиента, организую свободное движение животных, обеспечивая их здоровья и оптимизируя систему для работы доярок. При этом учитываются все факторы и требования(Расположение коровников, кормовых столов. навозных проходов и многое другое). Наша сервисная команда будет поддерживать Вас на каждой стадии проектирования.<br><br>Кроме того наша фирма занимается поставкой коров, доильного оборудования ( залы, передвижные мини-доильники), охладительных ёмкостей, кормораздатчиков и прочего сельскохозяйственного оборудования всех видов.</p>
          <?php elseif(\Yii::$app->language=='uz'): ?>
            <p> Kompaniyamiz sutni qayta ishlaydigan yangi sut sog'ish uskunalarini ishlab chiqarish yoki modernizatsiyalash bilan shug'ullanadi. Tashkilotimiz mutaxassislari o'zingizning biznesingiz, jumladan, barcha funktsional funktsiyalar va turli xil ehtiyojlar uchun individual konsepsiya ishlab chiqishi mumkin. Tashkilotimiz qadimgi optimallashtirishning barcha bosqichlarida sizga yordam beradi va yangi sut fermasining qurilishi. Siz dizaynerlar guruhiga, dizayndagi ko'p yillik tajribaga va chorva mollari uchun muvaffaqiyatli tugallangan loyihalarga ishonishingiz mumkin. Eng yaxshi yordamga ega bo'lasiz! Tashkilotimiz sut va sog'ish sexlarini professional va yuqori sifatli dizayni bo'yicha xizmatlarni Evropa standartlariga muvofiq noldan boshlab beradi. Mutaxassislarimiz mijozning individual talablariga muvofiq sut fermasini loyihalashtirib, hayvonlarning erkin harakatini tashkil etish, ularning sog'lig'ini ta'minlash va sutkashlar uchun tizimni optimallashtirish. Barcha omillar va talablar hisobga olinadi (go'sht maydonchalarini joylashtirish, go'ng yo'llari uchun qattiq jadvallar va boshqalar). Sizga xizmat ko'rsatuvchi jamoa Sizni har bir dizayn bosqichida qo'llab-quvvatlaydi. <br><br> Bundan tashqari, bizning kompaniya sigirlarni, sog'ish uskunalarini (zallarni, mobil mini-sutkalarni), sovutish uchun mo'ljallangan idishlar, distribyutorlar va barcha turdagi qishloq xo'jaligi texnikalarini etkazib berish bilan shug'ullanadi
            </p>
          <?php endif; ?>
          <div class="row">
            <div class="col-md-6">
              <?php if (\Yii::$app->language == 'uz'): ?>
                <ul class="arrow-style">
                  <li>1.Фермалар</li>
                  <li>2.Сутни кайта ишлаш</li>
                  <li>3.Озик-овкат саноати</li>
                  <li>4.Нефтегаз ва совутиш ускуналари</li>
                </ul>
              <?php elseif (\Yii::$app->language == 'ru'): ?>
                <ul class="arrow-style">
                  <li>1.Фермы</li>
                  <li>2.Переработка молока</li>
                  <li>3.Пищевая промышленность</li>
                  <li>4.Нефтегазовое и охладительное оборудование</li>
                </ul>
              <?php endif; ?>
            </div>
            <div class="col-md-6">
              <?php if (\Yii::$app->language == 'uz'): ?>
                <ul class="arrow-style">
                  <li>5.Сервис хизмат курсатиш ва оригинал эхтиет кисмлар</li>
                  <li>6.Фермер хужаликларни монтаж килиш</li>
                  <li>7.Сут фермер хужаликларига хизмат курсатиш</li>
                  <li>8.Мутахассисларни таййорлаш</li>
                </ul>
              <?php elseif (\Yii::$app->language == 'ru'): ?>
                <ul class="arrow-style">
                  <li>5.Сервисное обслуживание и оригинальные запчастные части</li>
                  <li>6.Монтаж ферм</li>
                  <li>7.Обслуживание молочной фермы</li>
                  <li>8.Обучение специалистов</li>
                </ul>
              <?php endif; ?>
            </div>
          </div>
          <h3>
            <a href="" class="hover">
              Прайслист
              <i class="icon-file-pdf red"></i>
            </a>
          </h3>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="item-box bottom-pad-10">
              <a>
                <i class="icon-star13 i-5x bg-color"></i>
                <h4>
                  <?php if (\Yii::$app->language == 'uz'): ?>
                    Бизнинг ишимиз
                  <?php elseif (\Yii::$app->language == 'ru'): ?>
                    Наша работа
                  <?php endif; ?>
                </h4>
                <p>
                  <?php if (\Yii::$app->language == 'uz'): ?>
                    Кўп турдаги воситалар ичидан, ҳақиқатдан фойда берадиганларини танланг!Чорвачилик комплексларини  профессионал жиҳозлаш.
                  <?php elseif (\Yii::$app->language == 'ru'): ?>
                    Выбирайте из широкого спектра инструментов, чтобы действительно выиграть от профессионального развития животноводческих комплексов.
                  <?php endif; ?>
                </p>
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="item-box bottom-pad-10">
              <a>
                <i class="icon-heart18 i-5x bg-color"></i>
                <h4>
                  <?php if (\Yii::$app->language == 'uz'): ?>
                    Нега мижозлар бизни маъқул кўради?
                  <?php elseif (\Yii::$app->language == 'ru'): ?>
                    Почему клиенты Выбирают нас?
                  <?php endif; ?>
                </h4>
                <p>
                  <?php if (\Yii::$app->language == 'uz'): ?>
                    Бизнинг мутахассисларимиз GEA Farm Technologies корхонасининг энг сифатли материалларидан фойдаланган холда,мижозларнинг хизматида бўлади
                  <?php elseif (\Yii::$app->language == 'ru'): ?>
                    Наши специалисты будут обслуживать наших клиентов, используя высококачественные материалы GEA Farm Technologies
                  <?php endif; ?>
                </p>
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="item-box bottom-pad-10">
              <a>
                <i class="icon-gift6 i-5x bg-color"></i>
                <h4>
                  <?php if (\Yii::$app->language == 'uz'): ?>
                    Биз нима таклиф қиламиз?
                  <?php elseif (\Yii::$app->language == 'ru'): ?>
                    Что мы предлагаем?
                  <?php endif; ?>
                </h4>
                <ul>
                  <?php if (\Yii::$app->language == 'uz'): ?>
                    <li>Германия махсулоти</li>
                    <li>Европа малакаси</li>
                    <li>Немисча сифат</li>
                    <li>Бутун дунё тан олган ишончли техника</li>
                    <li>Германияда ишлаб чиқарилган</li>
                  <?php elseif (\Yii::$app->language == 'ru'): ?>
                    <li>Немецкий продукт</li>
                    <li>Европейская квалификация</li>
                    <li>Немецкое качество</li>
                    <li>Надежная технология, признанная во всем мире</li>
                    <li>Сделано в Германии</li>
                  <?php endif; ?>
                </ul>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- clients -->
