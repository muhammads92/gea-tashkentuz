<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\JWPlayerAsset;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Главная';
?>

<?php require Yii::getAlias('@app').'/views/site/_slider.php'; ?>


<section id="services" class="page-section transparent">
  <div class="container">
    <div class="row">
      <div class="owl-carousel navigation-1 opacity text-left" data-pagination="false" data-items="3" data-autoplay="true" data-navigation="true">
        <?php $products= \app\models\Product::find()->all(); ?>
        <?php foreach ($products as $product): ?>
          <div class="col-sm-4 col-md-4 col-xs-12" data-animation="fadeInUp">
            <p class="text-center">
              <a href="<?= Yii::getAlias('@web') ?>/files/photo/products/<?=$product->images?>" class="opacity" data-rel="prettyPhoto[portfolio]">
                <img src="<?= Yii::getAlias('@web') ?>/files/photo/products/bigthumb/<?=$product->images?>" width="300" height="200" style="max-height:200px" />
            </a>
        </p>
        <h3>
          <a href="<?= Url::to('site/product/') ?>">
            <?php if (\Yii::$app->language == 'uz'): ?>
              <?= $product->title_uz?>
          <?php elseif (\Yii::$app->language == 'ru'): ?>
              <?= $product->title_ru?>
          <?php endif; ?>
      </a>
  </h3>
  <div class="eqh">
      <?php if (\Yii::$app->language == 'uz'): ?>
        <p><?= $product->description_uz?></p>
    <?php elseif (\Yii::$app->language == 'ru'): ?>
        <p><?= $product->description_ru?></p>
    <?php endif; ?>
</div>
<a href="<?= Url::to('site/product/') ?>" class="btn btn-default">
  <?php if (\Yii::$app->language == 'uz'): ?>
    Батафсил
<?php elseif (\Yii::$app->language == 'ru'): ?>
    Подробно
<?php endif; ?>
</a>
</div>
<?php endforeach ?>
</div>
</div>
</div>
</section>

<script type="text/javascript">
    <?php ob_start() ?>
    $('.eqh').matchHeight();
    <?php $this->registerJs(ob_get_clean()) ?>
</script>
<section id="who-we-are" class="page-section light-bg border-tb">
  <div class="container who-we-are">
    <div class="section-title text-left">
      <h2 class="title">
          <?php if (\Yii::$app->language == 'uz'): ?>
            Биз ҳақимизда
          <?php elseif (\Yii::$app->language == 'ru'): ?>
              О нас
          <?php endif ?>
      </h2>
  </div>
  <div class="row">
      <div class="col-md-4">
        <div class="item-box bottom-pad-10">
          <a>
            <i class="icon-star13 i-5x bg-color"></i>
            <h4>
              <?php if (\Yii::$app->language == 'uz'): ?>
                Бизнинг ишимиз?
            <?php elseif (\Yii::$app->language == 'ru'): ?>
                Что мы делаем?
            <?php endif; ?>
                
            </h4>
            <p><?php if (\Yii::$app->language == 'uz'): ?>
                Кўп турдаги воситалар ичидан, ҳақиқатдан фойда берадиганларини танланг!Чорвачилик комплексларини  профессионал жиҳозлаш.
            <?php elseif (\Yii::$app->language == 'ru'): ?>
                Выбирайте из широкого спектра инструментов, чтобы действительно выиграть от профессионального развития животноводческих комплексов.
            <?php endif; ?>
        </p>
    </a>
</div>
</div>
<div class="col-md-4">
    <div class="item-box bottom-pad-10">
      <a>
        <i class="icon-heart18 i-5x bg-color"></i>
        <h4>
          <?php if (\Yii::$app->language == 'uz'): ?>
            Нега мижозлар бизни маъқул кўради?
        <?php elseif (\Yii::$app->language == 'ru'): ?>
            Почему нам нравится?
        <?php endif; ?>
    </h4>
    <p><?php if (\Yii::$app->language == 'uz'): ?>
        Бизнинг мутахассисларимиз GEA Farm Technologies корхонасининг энг сифатли материалларидан фойдаланган холда,мижозларнинг хизматида бўлади
    <?php elseif (\Yii::$app->language == 'ru'): ?>
        Наши специалисты будут обслуживать наших клиентов, используя высококачественные материалы GEA Farm Technologies
    <?php endif; ?>
</p>
</a>
</div>
</div>
<div class="col-md-4">
  <div class="item-box bottom-pad-10">
    <a>
      <i class="icon-gift6 i-5x bg-color"></i>
      <h4>
        <?php if (\Yii::$app->language == 'uz'): ?>
          Биз нима таклиф қиламиз?
      <?php elseif (\Yii::$app->language == 'ru'): ?>
          Что мы предлагаем?
      <?php endif; ?></h4>
      <p>
          <ul>
            <li>
              <?php if (\Yii::$app->language == 'uz'): ?>
                Германия махсулоти
            <?php elseif (\Yii::$app->language == 'ru'): ?>
                Немецкое производство
            <?php endif; ?></li>
            <li>
                <?php if (\Yii::$app->language == 'uz'): ?>
                  Европа малакаси
              <?php elseif (\Yii::$app->language == 'ru'): ?>
                  Европейская квалификация
              <?php endif; ?></li>
              <li>
                  <?php if (\Yii::$app->language == 'uz'): ?>
                    Немисча сифат
                <?php elseif (\Yii::$app->language == 'ru'): ?>
                    Немецкое качество
                <?php endif; ?>

            </li>
            <li>
                <?php if (\Yii::$app->language == 'uz'): ?>

                  Бутун дунё тан олган ишончли техника
              <?php elseif (\Yii::$app->language == 'ru'): ?>
                  Надежная техника, признанная во всем мире
              <?php endif; ?>

          </li>
          <li>Германияда ишлаб чиқарилган
              <?php if (\Yii::$app->language == 'uz'): ?>
                Германияда ишлаб чиқарилган

                Бутун дунё тан олган ишончли техника
            <?php elseif (\Yii::$app->language == 'ru'): ?>
               Надежная техника, признанная во всем мире
           <?php endif; ?>

       </li>




   </ul> 

   <li>
    <?php if (\Yii::$app->language == 'uz'): ?>
      Германияда ишлаб чиқарилган

  <?php elseif (\Yii::$app->language == 'ru'): ?>
    Сделано в Германии
<?php endif; ?></li>



</ul>


</p>
</a>

</div>
</div>
</div>
</div>
</section>
<section id="works" class="page-section transparent">
    <div class="container">
      <div class="section-title text-left">
        <h2 class="title"><?= Yii::t('template', 'Галерея') ?></h2>
    </div>
</div>
<?= \app\components\Gallery::widget(); ?>
</section>
              <!--
              <section id="latest-news" class="page-section light-bg border-tb">
              <div class="container">
              <div class="section-title text-left">
              <h2 class="title">Видео галерея</h2>
            </div>
          </div>
          <?//= \app\components\Video::widget(); ?>
      </section> -->
