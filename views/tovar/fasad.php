        <!-- Sticky Navbar -->
        <div class="page-header page-title-left page-title-pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="title">Фасадлар</h1>
                        <h5>Турлари ва қулланилиши.</h5>
                    
                    </div>
                </div>
            </div>
        </div>
        <!-- page-header -->
        <section id="services" class="page-section service-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-9 col-md-push-3">
                        <div class="row">
                            <div class="col-md-12 content-block">
                                <div class="text-center">
                                    <div class="owl-carousel navigation-4" data-pagination="false" data-items="1"
                                    data-singleitem="true" data-autoplay="true" data-navigation="true">
                                    <a href="<?= Yii::getAlias('@web')?>/img/sections/services/7.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/7.jpg" width="1000" height="500" alt="" />
                                    </a> 
                                    <a href="<?= Yii::getAlias('@web')?>/img/sections/services/8.jpeg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/8.jpg" width="1000" height="500" alt="" />
                                    </a>
                                    <a href="<?= Yii::getAlias('@web')?>/img/sections/services/9.jpeg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/9.jpg" width="1000" height="500" alt="" />
                                    </a> 
                                    <a href="<?= Yii::getAlias('@web')?>/img/sections/services/10.jpeg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/10.jpg" width="1000" height="500" alt="" />
                                    </a>
                                    <a href="<?= Yii::getAlias('@web')?>/img/sections/services/11.jpeg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/11.jpg" width="1000" height="500" alt="" />
                                    </a> 
                                    <a href="<?= Yii::getAlias('@web')?>/img/sections/services/12.jpeg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/12.jpg" width="1000" height="500" alt="" />
                                    </a></div>
                                </div>
                                <p>
                                Бинолар қурилган кўпгина қурилиш материаллари қўшимча ишлов беришни талаб қилади. Ҳар бир инсон бошқаларига ўхшамайдиган гўзал уйда яшашни хоҳлайди. Уйнинг фасадини қўллар билан тугатгандан кейингина - бу материалда айтиб ўтамиз. Бинонинг жабҳаси эгаси ва унинг ҳолати ҳақида кўп нарсаларни айтиб бериши мумкин. Ҳар қандай кучли компания ўз биноси олдида идеал ҳолатни кузатиб боради. Яхшиямки, ҳозирда ҳар қандай маҳсулотни чунтакка қараб танлаш учун жуда кўп имкониятлар мавжуд, уйнинг чиройли кўринишини қандай безаш керак.</p>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-6 content-block opacity">
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/2.jpg" width="1000" height="500" alt="" />
                                <h4>Панел фасад</h4>
                                <p>Исталган  барча рангларда бўялган, фойдаланишга қулай рекорд вақт фойдаланишга эга. </p>
                            </div>
                            <div class="col-md-6 content-block opacity">
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/services/single/1.jpg" width="1000" height="500" alt="" />
                                <h4>Сайдинг фасад</h4>
                                <p>Сравнительная характеристика стеновых сэндвич-панелей с другими стеновыми материалами по теплопроводности

                            </p>

                            <ul><h5>Фасадлар</h5>
                                <li>Панелли фасад  </li>
                                <li>Сайдинг фасад</li>
                                <li>Ёғоч-рангли фасад</li>
                                
                              
                              
                            </ul>

                            </div>
                        </div>
                        <hr class="top-margin-0" />
                        <div class="row">
                            <div class="col-md-6 service-list content-block">
                                <h4>Нима учун бизни танлашади?</h4>
                                <ul>
                                    <li>
                                        <i class="icon-alarm3 text-color"></i>
                                        <p>биз дам олиш кунларисиз ишлаймиз</p>
                                    </li>
                                    <li>
                                        <i class="icon-shield2 text-color"></i>
                                        <p>Узоқ йиллик тажриба егамиз.</p>
                                    </li>
                            </div>
                            <div class="col-md-6 content-block">
                                <div role="tabpanel">
                                    <!-- Nav tabs -->
                                   
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active top-margin-0" id="brochure">
                                            <a href="#">
                                                <img src="<?= Yii::getAlias('@web')?>/img/sections/services/15.png" width="450" height="270" alt="" />
                                            </a>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="reporting">
                                            <p>Lorem ipsum dgggolor sit amet, consectetur adipisicing elit. Repudiandae odit iste
                                            exercitationem praesentium deleniti.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit nostrum laborum rem id
                                            nihil tempora.</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="inspection">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste
                                            exercitationem praesentium deleniti.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit nostrum laborum rem id
                                            nihil tempora.</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="others">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste
                                            exercitationem praesentium deleniti.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit nostrum laborum rem id
                                            nihil tempora.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar col-sm-12 col-md-3 col-md-pull-9">
                        <div class="widget list-border">
                            <div class="widget-title">
                                <h3 class="title">Ҳамма Товарлар</h3>
                            </div>
                            <div id="MainMenu1">
                                <div class="list-group panel">
                                    <div class="collapse in" id="demo">
                                    <a href="<?=Yii::getAlias('@web')?>/view/site/profnastil" class="list-group-item">Профнастил</a> 
                                    <a href="tunukapon.php" class="list-group-item">Тунукапон</a> 
                                    <a href="sendvich.php" class="list-group-item">Сендвич панеллар</a> 
                                    <a href="fasad.php" class="list-group-item">Потокли ва Фасадли</a> 
                                    <a href="panjara.php" class="list-group-item active">Панжара</a> 
                                    <a href="cherepitsa.php" class="list-group-item">Черепица</a></div>
                                </div>
                            </div>
                            <!-- category-list -->
                        </div>
                        <div class="widget">
                            <div class="widget-title">
                                <h3 class="title"></h3>
                            </div>
                          
                            <div class="widget">
                                <div class="widget-title">
                                    <h3 class="title">Фасадлар</h3>
                                </div>
                                <div class="owl-carousel navigation-4" data-pagination="false" data-items="1" data-autoplay="true"
                                data-navigation="true">
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/4.jpg" width="270" height="270" alt="" /> 
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/5.jpg" width="270" height="270" alt="" />
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/6.jpg" width="270" height="270" alt="" /> 
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/7.jpg" width="270" height="270" alt="" />
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/8.jpg" width="270" height="270" alt="" /> 
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/9.jpg" width="270" height="270" alt="" />
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/10.jpg" width="270" height="270" alt="" /> 
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/11.jpg" width="270" height="270" alt="" />
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/12.jpg" width="270" height="270" alt="" /> 
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/13.jpg" width="270" height="270" alt="" />
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/14.jpg" width="270" height="270" alt="" /> 
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/15.jpg" width="270" height="270" alt="" />
                                 <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/16.jpg" width="270" height="270" alt="" /> 
                                <img src="<?= Yii::getAlias('@web')?>/img/sections/blog/17.jpg" width="270" height="270" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Services -->
        <div id="get-quote" class="bg-color black text-center">
            <div class="container">
                <div class="row get-a-quote">
                    <div class="col-md-12">Get A Free Quote / Need a Help ? 
                    <a class="black" href="#">Contact Us</a></div>
                </div>
                <div class="move-top bg-color page-scroll">
                    <a href="#page">
                        <i class="glyphicon glyphicon-arrow-up"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- request -->
       