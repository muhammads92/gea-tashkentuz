<?//=Yii::$app->language?>
 <div class="container shop">
                <div class="section-title text-left">
                    <h2 class="title">Тунукапон</h2>
                </div>
                <div class="row">
                    <div class="owl-carousel navigation-1 opacity text-left" data-pagination="false" data-items="4"
                    data-autoplay="true" data-navigation="true">
                        <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="shop-single.html">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/1.jpg" alt="" width="350" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="#"><h4>Тунукапон потолок</h4></a>
                                    <h5 class="text-color">$199</h5>
                                </div>
                              
                            </div>
                        </div>
                        <!-- .product -->
                        <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="#">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/2.jpg" alt="" width="350" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="shop-single.html"><h4>Тунукапон Карниз</h4></a>
                                    <h5 class="text-color">$229</h5>
                                </div>
                              
                            </div>
                        </div>
                        <!-- .product -->
                        <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="shop-single.html">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/3.jpg" alt="" width="350" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="shop-single.html"><h4>Тунукапон Фасад</h4></a>
                                    <h5 class="text-color">$189</h5>
                                </div>
                              
                            </div>
                        </div>
                        <!-- .product -->
                        <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="shop-single.html">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/4.jpg" alt="" width="350" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="shop-single.html"><h4>Тунукапон Тумба</h4></a>
                                    <h5 class="text-color">$79</h5>
                                </div>
                              
                            </div>
                        </div>
                          <div class="col-md-3 col-sm-6">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="shop-single.html">
                                        <img src="<?= Yii::getAlias('@web')?>/img/sections/shop/5.jpg" alt="" width="400" height="400" />
                                    </a>
                                </div>
                                <div class="product-details">
                                    <a href="shop-single.html"><h4>Бошқа турлари</h4></a>
                                    <h5 class="text-color">$79</h5>
                                </div>
                              
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <div role="tabpanel" style="margin:50px;">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Та'риф</a>
                                        </li>
                                     
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="margin: 30px ;">
                                        <div role="tabpanel" class="tab-pane active" id="home">
                                            <h5>Та'риф</h5>
                                            <p>туникапон из листового металла </p>
                                            <p>Профнастил </p>
                                            <ul class="list-style">
                                                <li>По области применения: фасадные (стеновой, для забора), кровельные, несущие;</li>
                                                <li>По форме гофры (волнообразная, трапециевидная);</li>
                                                <li>По высоте гофры от 8 до 21 мм — стеновой профлист, применяемый для отделки стен, заборов и ворот; более 44 мм — профлист, применяемый для настила кровли и там, где предполагается существенная нагрузка; более 57 мм — профнастил под несъёмную опалубку (межэтажные и монолитные перекрытия);</li>
                                                <li>По ширине профиля;</li>
                                                <li>По толщине листа (от 0,3 до 1,5 мм);</li>
                                                <li>По полной и полезной ширине листа.</li>
                                            </ul>
                                            <p>При производстве профилированных листов используется листовая оцинкованная сталь с нанесенным на неё защитным полимерным или лакокрасочным покрытием по таблице RAL и RR, либо без защитного покрытия. Полимерное покрытие несёт защитную функцию (предотвращает появление коррозии), эстетическую функцию (богатство цветовых решений позволяет использовать профилированные листы при строительстве различных архитектурных объектов).</p>
                                            <p>Виды полимерного покрытия профилированного листа:</p>
                                            <ul class="list-style">
                                            <li>акрил — акрилатное покрытие(AK);</li>
                                            <li>полиэстер — полиэфирное покрытие(PE);</li>
                                            <li>поливинилхлорид — пластизолевое покрытие (PVC);</li>
                                            <li>поливинилиденфторидные покрытие (PVDF);</li>
                                            <li>полиуретановое покрытие (PUR).</li>
                                            </ul>
                                        </div>
                                       
                               
                             
                    
                                    <div class="page-header page-title-left page-title-pattern">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="title">Рулонлар</h1>
                        <h5>Лист</h5>
                    
                    </div>
                </div>
            </div>
        </div>


         <section id="who-we-are" class="page-section no-pad border-tb">
            <div class="container-fluid who-we-are">
                <div class="row">
                    <div class="col-md-8 pad-60 xs-pad-0 bottom-pad-10 top-pad-80">
                        <div class="owl-carousel navigation-3 opacity text-left" data-pagination="false" data-items="2"
                        data-autoplay="true" data-navigation="true">
                            <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/1.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/1.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Рулонлар</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/2.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/2.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">OUR MISSION</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/3.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/3.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">COMPANY VISION</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/4.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/4.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Operational Range</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/5.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/5.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Financial Information</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>
                               <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/6.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/6.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Financial Information</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>
                               <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/7.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/7.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Financial Information</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>
                               <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/8.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/8.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Financial Information</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>
                               <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/9.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/9.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Financial Information</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>

                               <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/10.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/10.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Financial Information</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>

                               <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/11.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/11.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Financial Information</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>

                               <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/12.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/12.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Financial Information</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>
                               <div class="col-sm-4 col-md-4 col-xs-12">
                                <p class="text-center">
                                    <a href="<?=Yii::getAlias('@web')?>/img/sections/about/13.jpg" class="opacity" data-rel="prettyPhoto[portfolio]">
                                        <img src="<?=Yii::getAlias('@web')?>/img/sections/about/13.jpg" width="370" height="185" alt="" />
                                    </a>
                                </p>
                                <h3>
                                    <a href="#">Financial Information</a>
                                </h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare odio et massa dignissim,
                                at accumsan metus viverra. Integer neque lectus, pellentesque sit dolor sit dolor.</p>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 no-pad text-center">
                        <!-- Image -->
                        <div class="image-bg" data-background="<?=Yii::getAlias('@web')?>/img/sections/bg/1.jpg"></div>
                    </div>
                </div>
            </div>
        </section></div>
        <!-- who-we-are -->
        
      