<?php
use yii\helpers\Url;
?>

<div id="get-quote" class="bg-color black text-center">
    <div class="container">
        <div class="row get-a-quote">
            <div class="col-md-12">
                <a class="black" href="<?= Url::to('site/contact') ?>">
                  <?php if (\Yii::$app->language == 'uz'): ?>
                    Биз билан богланинг
                  <?php elseif (\Yii::$app->language == 'ru'): ?>
                    Связаться с нами
                  <?php endif; ?>
                </a>
            </div>
        </div>
        <div class="move-top bg-color page-scroll">
            <a href="#page">
                <i class="glyphicon glyphicon-arrow-up"></i>
            </a>
        </div>
    </div>
</div>
<footer id="footer">
    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                    <div class="widget-title">
                        <h3 class="title">
                          <?php if (\Yii::$app->language == 'ru'): ?>
                              Наши услуги
                          <?php elseif (\Yii::$app->language == 'uz'): ?>
                              Бизнинг хизматлар
                          <?php endif; ?>
                        </h3>
                    </div>
                    <nav>
                        <ul>
                            <li>
                              <?php if (\Yii::$app->language == 'ru'): ?>
                                  Обслуживание молочной фермы
                              <?php elseif (\Yii::$app->language == 'uz'): ?>
                                  Сут фермаси хизматлари
                              <?php endif; ?>
                            </li>
                        <ul>
                            <li>
                              <?php if (\Yii::$app->language == 'ru'): ?>
                                  Ввод в эксплуатацию
                              <?php elseif (\Yii::$app->language == 'uz'): ?>
                                  Ишга тушириш
                              <?php endif; ?>
                            </li>
                        <ul>
                            <li>
                              <?php if (\Yii::$app->language == 'ru'): ?>
                                  Готовые решения
                              <?php elseif (\Yii::$app->language == 'uz'): ?>
                                  Таййор ечимлар
                              <?php endif; ?>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                    <div class="widget-title">
                        <h3 class="title">Адрес</h3>
                    </div>
                    <p>
                      <?php if (\Yii::$app->language == 'uz'): ?>
                          Қатортол кўч., 1А /2 уй <br>
                          Тошкент ш., Чилонзор тумани
                      <?php elseif (\Yii::$app->language == 'ru'): ?>
                          ул. Катартал, дом 1А /2 <br>
                          г. Ташкент, Чиланзарский район
                      <?php endif; ?>
                    </p>

                    </div>
                <div class="col-xs-12 col-sm-6 col-md-3 widget bottom-xs-pad-20">
                    <div class="widget-title">
                        <h3 class="title">Контакты</h3>
                    </div>
                    <p><strong> +998 (71) 276-96-91<br /> +998 (97) 773-09-99</strong></p>
                    <a class="text-color" href="mailto:gea.tashkent@gmail.com">gea.tashkent@gmail.com</a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 widget">
                    <div class="widget-title">
                        <!-- Title -->
                        <h3 class="title">
                          <?php if (\Yii::$app->language == 'ru'): ?>
                            Время работы
                          <?php elseif (\Yii::$app->language == 'uz'): ?>
                            Иш вақти
                          <?php endif; ?>
                        </h3>
                    </div>
                    <nav>
                        <ul>
                            <!-- List Items -->
                            <li>
                                <?php if (\Yii::$app->language == 'ru'): ?>
                                  с 9:00 до 18:00
                                <?php elseif (\Yii::$app->language == 'uz'): ?>
                                  9:00 дан 18:00 гача
                                <?php endif; ?>
                            </li>
                            <li>
                              <?php if (\Yii::$app->language == 'uz'): ?>
                                Дам олиш кунисиз
                              <?php elseif (\Yii::$app->language == 'ru'): ?>
                                Без выходных
                              <?php endif; ?>
                            </li>
                        </ul>
                        <!-- Count -->
                        <!-- <div class="footer-count">
                            <p class="count-number" data-count="3550">total projects :
                                <span class="counter"></span>
                            </p>
                        </div>
                        <div class="footer-count">
                            <p class="count-number" data-count="2550">happy clients :
                                <span class="counter"></span>
                            </p>
                        </div> -->
                    </nav>
                </div>
     <!--            <div class="col-xs-12 col-sm-6 col-md-3 widget newsletter bottom-xs-pad-20">
                    <div class="widget-title">
                        <h3 class="title">Подписка</h3>
                    </div>
                    <div>
                        <p>Subscribe to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</p>
                        <p class="form-message1"></p>
                        <div class="clearfix"></div>
                        <form id="subscribe_form" action="subscription.php" method="post" name="subscribe_form"
                              role="form">
                            <div class="input-text form-group has-feedback">
                                <input class="form-control" type="email" value="" name="subscribe_email" />
                                <button class="submit bg-color" type="submit">
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                </button></div>
                        </form>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <!-- footer-top -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    &copy; GEA-Tashkent, 2018
                </div>
            </div>
        </div>
    </div>
    <!-- footer-bottom -->
</footer>
<!-- footer -->
