<?php

use app\components\Helper;// o`zgartirilganda o`zgarmadi
?>

<div class="new-version tb-pad-20">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="navbar-header">
                        <!-- Button For Responsive toggle -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span></button>
                        <!-- Logo -->
                        <a class="navbar-brand" href="<?=\Yii::$app->homeUrl?>">
                            <img class="site_logo" alt="Site Logo" src="<?= Yii::getAlias('@web')?>/img/logo.png"/>
                        </a>
                    </div>
                        <!-- Navbar Collapse -->
                        <div class="office-details">
                            <div class="detail-box">
                                <div class="icon"><i class="fa fa-phone"></i></div>
                                <div class="detail">
                                    <strong>+998 (71) 276-96-91<br>+998 (97) 773-09-99</strong>
                                    <!-- <span>
                                        <a href="mailto:gea.tashkent@gmail.com">gea.tashkent@gmail.com
</a>
                                    </span> -->
                                </div>
                            </div>
                            <div class="detail-box">
                                <div class="icon"><i class="fa fa-clock-o"></i></div>
                                <div class="detail">
                                    <strong>
                                      <?php if (\Yii::$app->language == 'uz'): ?>
                                        Иш вакти 9:00 дан 18:00 гача
                                      <?php elseif (\Yii::$app->language == 'ru'): ?>
                                        Время работы с 9:00 до 18:00
                                      <?php endif; ?>
                                    </strong>
                                    <span>
                                      <?php if (\Yii::$app->language == 'uz'): ?>
                                        Дам олишсиз ишлаймиз!
                                      <?php elseif (\Yii::$app->language == 'ru'): ?>
                                        Без выходных
                                      <?php endif; ?>
                                    </span>
                                </div>
                            </div>
                            <div class="detail-box">
                                <div class="icon"><i class="fa fa-map-marker"></i></div>
                                <div class="detail">
                                  <?php if (\Yii::$app->language == 'uz'): ?>
                                      <strong>Қатортол кўч., 1А /2 уй</strong><span> Тошкент ш., Чилонзор тумани</span>
                                  <?php elseif (\Yii::$app->language == 'ru'): ?>
                                      <strong>ул. Катартал, дом 1А /2</strong><span> г. Ташкент, Чиланзарский район</span>
                                  <?php endif; ?>
                                </div>
                            </div>


                            <div class="col-sm-4 hidden-xs">
                              <span class="top-welcome">

  <!-- <a href="<?=\app\components\Helper::createMultilanguageReturnUrl('uz') ?>">O'zb</a>
|
<a href="<?=\app\components\Helper::createMultilanguageReturnUrl('cyrl') ?>">cyrl</a>

<a href="<?=\app\components\Helper::createMultilanguageReturnUrl('ru') ?>">рус</a>
|
<a href="<?=\app\components\Helper::createMultilanguageReturnUrl('en') ?>">Eng</a>  -->
                        </span>
                    </div>



                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.col-md-12 -->
                </div>
            </div>
        </div>



<!-- Top Bar --><!--
<div id="top-bar" class="top-bar-section top-bar-bg-light">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <!-- Top Contact --><!--
                <div class="top-contact link-hover-black">
                    <a href="#"><i class="fa fa-phone"></i>(98)311-84-83, (98)309-71-09</a>
                    <a href="#"><i class="fa fa-envelope"></i>info@tombaraka.com</a>
                </div>

                <!-- Top Social Icon --><!--
                <div class="top-social-icon icons-hover-black">
                    <a href="#">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-youtube"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-rss"></i>
                    </a>
                </div>


            </div>
        </div>
    </div>
</div> -->
