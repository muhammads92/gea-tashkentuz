<?php ?>

<!-- Top Bar -->
<!-- Sticky Navbar -->
<header id="sticker" class="sticky-navigation">
    <!-- Sticky Menu -->
    <div class="sticky-menu relative">
        <!-- navbar -->
        <div class="navbar navbar-default navbar-bg-light" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <!-- Button For Responsive toggle -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span>
                            </button> 
                            <!-- Logo -->

                            <a class="navbar-brand" href="index.php">
                                <img class="site_logo" alt="Site Logo" src="<?= Yii::getAlias('@web')?>/img/logo.png" />
                            </a>
                        </div>
                        <!-- Navbar Collapse -->
                        <div class="navbar-collapse collapse">
                            <!-- nav -->
                            <ul class="nav navbar-nav">
                                <!-- Home  Mega Menu -->
                                <li>
                                    <a href="#">Home</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="index1.html">Metal Home 1</a>
                                        </li>
                                        <li>
                                            <a href="index2.html">Metal Home 2</a>
                                        </li>
                                        <li>
                                            <a href="index3.html">Metal Home 3</a>
                                        </li>
                                        <li>
                                            <a href="index4.html">Metal Home 4</a>
                                        </li>
                                        <li>
                                            <a href="index5.html">Metal Home 5</a>
                                        </li>
                                        <li>
                                            <a href="index6.html">Metal Home 6</a>
                                        </li>
                                        <li>
                                            <a href="index7.html">Metal Home 7</a>
                                        </li>
                                        <li>
                                            <a href="index8.html">Metal Home 8</a>
                                        </li>
                                        <li>
                                            <a href="index9.html">Metal Home 9</a>
                                        </li>
                                        <li>
                                            <a href="index-onepage.html">Metal One Page</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- Mega Menu Ends -->
                                <!-- Features Menu -->
                                <li class="mega-menu">
                                    <a href="#">Features</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <!-- Home Mage Menu grids Begins -->
                                            <div class="row">
                                                <!-- Page Block -->
                                                <div class="col-sm-3">
                                                    <!-- Title -->
                                                    <h6 class="title">Pages</h6>
                                                    <!-- Links -->
                                                    <div class="page-links">
                                                        <div>
                                                            <a href="team.html">Team</a>
                                                        </div>
                                                        <div>
                                                            <a href="careers.html">Careers</a>
                                                        </div>
                                                        <div>
                                                            <a href="coming-soon.html">Coming Soon</a>
                                                        </div>
                                                        <div>
                                                            <a href="maintenance.html">Under Maintenance</a>
                                                        </div>
                                                        <div>
                                                            <a href="clients.html">Clients</a>
                                                        </div>
                                                        <div>
                                                            <a href="testimonials.html">Testimonials</a>
                                                        </div>
                                                        <div>
                                                            <a href="faq.html">FAQ&#39;s</a>
                                                        </div>
                                                        <div>
                                                            <a href="404.html">404 Page</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Page Block -->
                                                <!-- Header Block -->
                                                <div class="col-sm-3">
                                                    <!-- Title -->
                                                    <h6 class="title">Header</h6>
                                                    <!-- Links -->
                                                    <div class="page-links">
                                                        <div>
                                                            <a href="header1.html">Simple Header</a>
                                                        </div>
                                                        <div>
                                                            <a href="header2.html">Shop Header</a>
                                                        </div>
                                                        <div>
                                                            <a href="side-nav.html">Push Menu Header</a>
                                                        </div>
                                                        <div>
                                                            <a href="header3.html">Header Center</a>
                                                        </div>
                                                        <div>
                                                            <a href="header4.html">Toggle Light Header</a>
                                                        </div>
                                                        <div>
                                                            <a href="page-title-1.html">Page Title Simple</a>
                                                        </div>
                                                        <div>
                                                            <a href="page-title-2.html">Page Title Parallax</a>
                                                        </div>
                                                        <div>
                                                            <a href="page-title-3.html">Page Title Pattern</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Header Block -->
                                                <!-- Slider Block -->
                                                <div class="col-sm-3">
                                                    <!-- Title -->
                                                    <h6 class="title">Slider</h6>
                                                    <!-- Links -->
                                                    <div class="page-links">
                                                        <div>
                                                            <a href="revolution-slider-1.html">Revolution Slider 1</a>
                                                        </div>
                                                        <div>
                                                            <a href="revolution-slider-2.html">Revolution Slider 2</a>
                                                        </div>
                                                        <div>
                                                            <a href="revolution-slider-3.html">Revolution Slider 3</a>
                                                        </div>
                                                        <div>
                                                            <a href="revolution-slider-4.html">Revolution Slider 4</a>
                                                        </div>
                                                        <div>
                                                            <a href="carousel-slider.html">Carousel Slider</a>
                                                        </div>
                                                        <div>
                                                            <a href="text-slider.html">Text Slider</a>
                                                        </div>
                                                        <div>
                                                            <a href="videobg-slider.html">Video Background</a>
                                                        </div>
                                                        <div>
                                                            <a href="parallax-slider.html">Parallax Background</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Slider Block -->
                                                <!-- Button Block-->
                                                <div class="col-sm-3 text-center xs-pad-30">
                                                    <img src="img/sections/man.jpg" alt="" /> 
                                                    <a href="" class="btn btn-default btn-block get-in-touch">Get In
                                                        Touch</a></div>
                                                <!-- Button Block -->
                                            </div>
                                            <!-- Ends Home Mage Menu Block -->
                                        </li>
                                    </ul>
                                </li>
                                <!-- Ends Features Menu -->
                                <!-- ABout Us -->
                                <li>
                                    <a href="#">About</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="about-us.html">About Us 1</a>
                                        </li>
                                        <li>
                                            <a href="about-us-2.html">About Us 2</a>
                                        </li>
                                        <li>
                                            <a href="about-us-3.html">About Us 3</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- Ends Widgets Block -->
                                <!-- Portfolio Menu -->
                                <li>
                                    <a href="#">Projects</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="portfolio.html">Grid</a>
                                        </li>
                                        <li>
                                            <a href="portfolio-list.html">List</a>
                                        </li>
                                        <li>
                                            <a href="portfolio-single.html">Single</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- Portfolio Menu -->
                                <!-- Service Menu -->
                                <li>
                                    <a href="services.html">Services</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="services-1.html">General Contracting</a>
                                        </li>
                                        <li>
                                            <a href="services-2.html">Construction Consultant</a>
                                        </li>
                                        <li>
                                            <a href="services-3.html">House Renovation</a>
                                        </li>
                                        <li>
                                            <a href="services-4.html">Green House</a>
                                        </li>
                                        <li>
                                            <a href="services-5.html">Tiling and Painting</a>
                                        </li>
                                        <li>
                                            <a href="services-6.html">Metal Roofing</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- Service Menu -->
                                <!-- Shop Menu -->
                                <li>
                                    <a href="#">Shop</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="shop-grid.html">Grid</a>
                                        </li>
                                        <li>
                                            <a href="shop-list.html">List</a>
                                        </li>
                                        <li>
                                            <a href="shop-single.html">Single</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- Ends Shop Menu -->
                                <!-- Blog Menu -->
                                <li>
                                    <a href="#">Blog</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="blog-grid.html">Grid</a>
                                        </li>
                                        <li>
                                            <a href="blog-list.html">List</a>
                                        </li>
                                        <li>
                                            <a href="blog-single.html">Single</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- Blog Menu -->
                                <!-- Contact Block -->
                                <li>
                                    <a href="contact.html">Contact</a>
                                </li>
                                <!-- Ends Contact Block -->
                                <!-- Header Search -->
                                <li class="hidden-767">
                                    <a href="#" class="header-search">
                                        <span>
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <!-- Right nav -->
                            <!-- Header Search Content -->
                            <div class="bg-white hide-show-content no-display header-search-content">
                                <form role="search" class="navbar-form vertically-absolute-middle">
                                    <div class="form-group">
                                        <input type="text" placeholder="Enter your text &amp; Search Here"
                                               class="form-control" id="s" name="s" value="" />
                                    </div>
                                </form>
                                <button class="close">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                            <!-- Header Search Content -->
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.col-md-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- navbar -->
    </div>
    <!-- Sticky Menu -->
</header>
