<?php

use app\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;
use yii2mod\notify\BootstrapNotify;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="utf-8" />

    <title><?=Html::encode($this->title) ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Metal — A Construction Company Template" />
    <meta name="author" content="zozothemes.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <script type="text/javascript">
        var siteBaseUrl = '<?= Yii::$app->homeUrl ?>';
        var siteLanguage = '<?= Yii::$app->language ?>';
        var speechNotification = '';
    </script>
    <?= Html::csrfMetaTags() ?>
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?= Yii::getAlias('@web')?>/img/favicon.ico" />
    <!-- Font -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Arimo:300,400,500,700,400italic,700italic' />
    
<!-- <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">    -->    

    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet"> 
    
    

    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div id="page">
        <!-- Page Loader -->
       

        <?php require_once Yii::getAlias('@app').'/views/blocks/preheader.php' ?>
        
        <?php require_once Yii::getAlias('@app').'/views/blocks/menu.php' ?>

        <?php echo $content; ?>

        <!-- clients -->
        
       <?php require_once Yii::getAlias('@app').'/views/blocks/footer.php' ?>
        

    </div>
    <!-- page -->
    <!-- Scripts -->
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>