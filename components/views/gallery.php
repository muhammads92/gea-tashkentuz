<?php
use yii\widgets\LinkPager;
use app\components\ImageResized;
?>
<div class="container general-section">

    <div id="options" class="filter-menu" style="margin-top: 50px;">
        <ul class="option-set black nav nav-pills">
            <li class="filter active" data-filter="all">
              <?php if (\Yii::$app->language =='uz'): ?>
                Барчаси
              <?php elseif (\Yii::$app->language =='ru'): ?>
                Все
              <?php endif; ?>
            </li>
            <?php foreach ($categories as $category): ?>
                <?php if (\Yii::$app->language == 'uz'): ?>
                  <li class="filter" data-filter=".category<?= $category['id'] ?>"><?= $category['name_uz'] ?></li>
                <?php elseif (\Yii::$app->language == 'ru'): ?>
                  <li class="filter" data-filter=".category<?= $category['id'] ?>"><?= $category['name_ru'] ?></li>
                <?php endif; ?>
              <?php endforeach; ?>
        </ul>
    </div>

</div>

<div class="container general-section white" style="margin-bottom: 50px;">
    <div id="mix-container" class="portfolio-grid custom no-item-pad">

        <?php foreach ($images as $photo):?>
            <div class="grids col-xs-12 col-sm-4 col-md-3 mix all category<?= $photo['category_id']?>">
                  <div class="grid">
                <div style="width: 275px; height: 188px; overflow: hidden;position: relative;">
                    <img style="position: absolute; top:-100%; left:0; right: 0; bottom:-100%; margin: auto; height:200px" src="<?= ImageResized::getResized('/files/photo/'.$photo['file'], null, 273)?>" width="400" height="273" alt="Recent Work" class="img-responsive" />
                </div>
                    <div class="figcaption">
                        <h4>GEA FARM</h4>
                        <!-- Image Popup-->
                        <a href="<?= ImageResized::getResized('/files/photo/'.$photo['file'], null, 200)?>" data-rel="prettyPhoto">
                          <!-- data-rel="prettyPhoto[portfolio]" -->
                            <i class="fa fa-search"></i>
                        </a>
                        <a href="portfolio-single.html">
                            <i class="fa fa-link"></i>
                        </a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>


    </div>
</div>
