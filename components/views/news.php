<?php foreach ($news as $new): ?>
    <div class="col-sm-4 col-md-4 col-xs-12">
        <p class="text-center">

            <?php
            if ($new->images) {
                $img_source = Yii::getAlias("@web/files/photo/news/{$new->images}");
                $thumb_source = Yii::getAlias("@web/files/photo/news/{$new->images}");
            } else {
                $img_source = Yii::getAlias('@web') . ("/img/default/news.jpg");
                $thumb_source = Yii::getAlias('@web') . ("/images/default/thumb/news.jpg");
            }
            ?>

            <a href="<?= $img_source ?>" class="opacity" data-rel="prettyPhoto[portfolio]">
                <img src="<?= $thumb_source ?>" width="420" height="280" alt="" />
            </a>
        </p>

        <h3>
            <a href="#"><?= $new['title_uz'] ?></a>
        </h3>
        <p><?= $new['content_uz'] ?></p>
    </div>
<?php endforeach; ?>