<?php
use yii\helpers\Html;
use yii\helpers\Url;

function menuTree($menu){
    //agar items element bo'lmasa menuTree ga $menus keldi degani
    if(!isset($menu['items'])){
        $tmenu = $menu;
        $menu = ['id' => 0];
        $menu['items'] = $tmenu;
        unset($tmenu);
    }
    if( !empty($menu['items']) ){

        $result = '';
        foreach($menu['items'] as $item){

            if($item['page_id']){
                $url = Url::to(['/page/view', 'id'=>$item['page_id']], true);
            }elseif($item['link']){
                $url =Yii::$app->linkCreator->create($item['link']);
            }else{
                $url = null;
            }

            if(isItemActive($item)){
                if(!empty($item['items'])){
                    if ($item['parent_id'] == 0) {
                        $result .= "<li class='dropdown active'>";
                    } else {
                        $result .= "<li class='dropdown-submenu active'>";
                    }
                }else{
                    $result .= "<li class='active'>";
                }
            }else{
                if(!empty($item['items'])){
                    if ($item['parent_id'] == 0) {
                        $result .= "<li class='dropdown'>";
                    } else {
                        $result .= "<li class='dropdown-submenu'>";
                    }
                }else{
                    $result .= "<li>";
                }
            }

            $result .= _view($item, $menu['id']);
            $child = menuTree($item);
            if($child){
                // @TODO
                $result .= "<ul class='dropdown-menu'>";
                $result .= $child;
                $result .= "</ul>";
            }
            $result .= "</li>";
        }
        return $result;
    }else{
        return '';
    }
}

function _view($item, $parentId){
    // @TODO
    $result = '';
    $options = [];

    if($item['page_id']){
        $url = Url::to(['/page/view', 'id'=>$item['page_id']]);
    }elseif($item['link']){
        $url =Yii::$app->linkCreator->create($item['link']);
    }else{
        $url = '#';
    }

    $class = '';
    if($url === '#'){
        $class .= '';
    }

    if(isset($item['name_'.Yii::$app->language])){
        $content = $item['name_'.Yii::$app->language];
    }else{
        $content = $item['name_uz'];
    }

    if(!empty($item['items'])){
        if($item['parent_id'] == 0){
            $class .= ' dropdown-toggle js-activated';
            $options['data-toggle'] = 'dropdown';
            $options['aria-haspopup'] = 'true';
//            $content .= ' <i class="fa fa-angle-down"></i>';
            $content .= ' <i class="fa fa-caret-down"></i>';
            $options['role'] = 'button';
        }else{
            $options['tabindex'] = '-1';
        }

    }
    if($item['target_blank']){
        $target_blank = TRUE;
    }else{
        $target_blank = FALSE;
    }

    $options['class'] = $class;
    if($target_blank){
        $options['target'] = 'blank';
    }

    $result .= Html::a($content, $url, $options);

    return $result;
}

function isItemActive($item) {
//    page_id, link
    if($item['page_id']){
        $url = Url::to(['/page/view', 'id'=>$item['page_id']]);

        if(strpos($_SERVER['REQUEST_URI'], $url) !== false && $item['page_id'] == $_REQUEST['id']){
            return true;
//        }elseif(strpos($url, $_SERVER['REQUEST_URI']) !== false){
//            return true;
        }else{
            return false;
        }

    }elseif(isset($item['link'])){
        $item['link'] = Yii::$app->linkCreator->createArr($item['link']);

        if (is_array($item['link'])) {

            if (isset($item['link'][0])) {

                $groute = null;
                if (Yii::$app->controller !== null) {
                    $groute = Yii::$app->controller->getRoute();
                }
                $gparams = Yii::$app->request->getQueryParams();

                $route = Yii::getAlias($item['link'][0]);

                if ($route[0] !== '/' && Yii::$app->controller) {
                    $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
                }

                $defaultRoute = Yii::$app->defaultRoute;
                if($defaultRoute == 'site'){
                    $defaultRoute = 'site/index';
                }
                if($route == '/' and $defaultRoute == $groute){
                    return true;
                }

                if (ltrim($route, '/') !== $groute) {
                    return false;
                }
                unset($item['link']['#']);

                if (count($item['link']) > 1) {
                    $params = $item['link'];
                    unset($params[0]);
                    foreach ($params as $name => $value) {
                        if ($value !== null && (!isset($gparams[$name]) || $gparams[$name] != $value)) {
                            return false;
                        }
                    }
                }

                return true;
            } else {
                return false;
            }
        }elseif(strpos($item['link'], 'http://') !== false || strpos($item['link'], 'https://') !== false || strpos($item['link'], 'ftp://') !== false){
            if (strpos($_SERVER['REQUEST_URI'], $item['link']) !== false || strpos($item['link'], $_SERVER['REQUEST_URI']) !== false) {
                return true;
            } else {
                return false;
            }
        }
    }

    return false;
}

?>

<ul class="nav navbar-nav full-width sm">
    <?= menuTree($menus); ?>
</ul>
