<?php

namespace app\components;

use Gumlet\ImageResize;
use Yii;

/**
 * Description of ImageResized
 *
 * @author muhammad
 */
class ImageResized {
    
    public static $quality = 85;

    //$returnType web|root
    //$mode safe|strict
    public static function getResized($imagePath, $width=null, $height=null, $returnType = 'web', $mode=null){

        if ($returnType == 'web' && $mode === null) {
            $mode = 'safe';
        } elseif ($returnType == 'root' && $mode === null) {
            $mode = 'strict';
        }
        
        $imagePath = ltrim($imagePath, '/');

        $imageFullPath = Yii::getAlias('@webroot').'/'.$imagePath;

        $compressedFileName = self::getCompressedFileName($imagePath, $width, $height);
        
        $compressedFileNameFullPath = Yii::getAlias('@webroot').'/'.$compressedFileName;
        
        if(file_exists($compressedFileNameFullPath)){
            
            return self::returnResult($compressedFileName, $returnType);
            
        }else{
            if($mode == 'safe' && !file_exists($imageFullPath)){
                return self::returnResult($imagePath, $returnType);
            }

            $image = new ImageResize($imageFullPath);

            $ext = self::getExtenstion($imagePath);

            if ($ext == 'jpg') {
                $image->quality_jpg = self::$quality;
            } elseif ($ext == 'png') {
                $image->quality_png = round((100 - self::$quality) * 0.09);
            }

            if ($width !== null and $height !== null) {
                $image->resize($width, $height);
            } elseif ($width !== null and $height === null) {
                $image->resizeToWidth($width);
            } elseif ($width === null and $height !== null) {
                $image->resizeToHeight($height);
            }
            
            if ($ext == 'jpg') {
                $image->save($compressedFileNameFullPath, IMAGETYPE_JPEG);
            } elseif ($ext == 'png') {
                $image->save($compressedFileNameFullPath, IMAGETYPE_PNG);
            }
            return self::returnResult($compressedFileName, $returnType);
        }
    }

    public static function returnResult($compressedFileName, $returnType){
        if($returnType == 'web'){
            return Yii::getAlias('@web').'/'.$compressedFileName;
        }else{
            //case root
            return Yii::getAlias('@webroot').'/'.$compressedFileName;
        }
    }
    
    public static function getCompressedFileName($imagePath, $width=null, $height=null){
        
        $compressedImageName = '';
        
        if($width !== null and $height !== null){
            $compressedImageName = 'w'.$width.'x'.'h'.$height.'x';
        }elseif($width !== null and $height === null){
            $compressedImageName = 'w'.$width.'x';
        }elseif($width === null and $height !== null){
            $compressedImageName = 'h'.$height.'x';
        }
        
        $filename = basename($imagePath);
        
        $compressedImageName .= $filename;
        
        return str_replace($filename, 'resized/'.$compressedImageName, $imagePath);
    }

    public static function getExtenstion($imagePath){
        
        $arr = explode('.', $imagePath);
        
        return strtolower(end($arr));
    }
}
