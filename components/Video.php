<?php

namespace app\components;

use yii\base\Widget;
use Yii;

/**
 * Description of Menu
 *
 * @author Muhammad
 */
class Video extends Widget {

    public $videos;

    public function init() {
        parent::init();

        $this->videos = \app\models\Video::find()->orderBy(['id' => SORT_DESC])->all();
    }

    public function run() {

        return $this->render('video', [
            'videos' => $this->videos
        ]);
    }

}
