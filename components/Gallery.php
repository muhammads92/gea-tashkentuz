<?php

namespace app\components;

use yii\base\Widget;
use app\models\Category;
use app\models\Photo;

/**
 * Description of Menu
 *
 * @author muhammad
 */
class Gallery extends Widget{
    public $categories;
    public $images;
    
    public function init(){
        parent::init();
        
        $this->categories = Category::find()->all();
        $this->images = Photo::find()->where(['chosed' => 1])->orderBy('category_id')->limit(12)->all();
  
    }
    public function run(){

    return $this->render('gallery', [
            'categories' => $this->categories,
            'images' => $this ->images
        ]);
    }
}
