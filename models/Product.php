<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $category_id
 * @property string $title_uz
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_cyrl
 * @property string $description_uz
 * @property string $description_ru
 * @property string $description_en
 * @property string $content_uz
 * @property string $content_ru
 * @property string $content_en
 * @property string $content_cyrl
 * @property string $images
 * @property int $slider
 * @property string $url
 */
class Product extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'slider'], 'required'],
            [['category_id'], 'integer'],
            ['url', 'safe'],
            [['title_uz', 'title_ru', 'title_en', 'title_cyrl', 'description_uz', 'description_ru', 'description_en', 'content_uz', 'content_ru', 'content_en', 'content_cyrl'], 'string'],
            [['images'], 'string', 'max' => 255],
            [['slider'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'title_uz' => 'Title Uz',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'title_cyrl' => 'Title Cyrl',
            'description_uz' => 'Description Uz',
            'description_ru' => 'Description Ru',
            'description_en' => 'Description En',
            'content_uz' => 'Content Uz',
            'content_ru' => 'Content Ru',
            'content_en' => 'Content En',
            'content_cyrl' => 'Content Cyrl',
            'images' => 'Images',
            'slider' => 'Slider',
            'url' => 'Url',
        ];
    }
}
