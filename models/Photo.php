<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%photo}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name_uz
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_cyrl
 * @property string $file
 * @property integer $chosed
 *
 * @property Category $category
 */
class Photo extends \yii\db\ActiveRecord
{
    public $fileFile;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'name_uz', 'name_ru', 'name_en', 'chosed'], 'required'],
            [['category_id', 'chosed'], 'integer'],
            [['fileFile'],'file'],
            [['name_uz', 'name_ru', 'name_en', 'fileFile'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Kategoriya',
            'name_uz' => 'Nomi Uz',
            'name_ru' => 'Nomi Ru',
            'name_en' => 'Nomi En',
            'file' => 'Rasm',
            'fileFile' => 'Rasm',
            'chosed' => 'Tanlangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
     public static function all($category_id = NULL){
        
        $query = self::find();
        if($category_id){
            $res = $query->where(['category_id', $category_id]);
        }
        $res = $query->orderBy('name_uz')->asArray()->all();
        
        return \yii\helpers\ArrayHelper::map($res, 'id', 'name_uz');
    }

    
    public static function searchAnywhere($keyword) {
        if (!empty($keyword)) {
            $query = self::find();

            $keyword = trim($keyword);
            $keyword = htmlentities($keyword);
            
            $query->select('*');                  // выбираем только поле 'title'
            $query->orWhere(['like', 'name_uz', $keyword])
            ->orWhere(['like', 'name_ru', $keyword])
            ->orWhere(['like', 'name_en', $keyword])
            //->orWhere(['like', 'name_cyrl', $keyword])
            ->orWhere(['like', 'file', $keyword])
            ->orWhere(['like', 'fileFile', $keyword])
            ->orWhere(['like', 'chosed', $keyword]);
            // ->orWhere(['like', 'content_en', $keyword]);
            //->orWhere(['like', 'content_cyrl', $keyword]);
            
            $modelNews = $query->asArray()->all();
        } else {
            $modelNews = FALSE;
        }
        return $modelNews;
    }



}
