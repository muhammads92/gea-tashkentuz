<?php
namespace app\models;

use yii\base\Model;
use Yii;

//use app\models\Video;
use app\models\Page;
//use app\models\Category;
use yii\data\ActiveDataProvider;
//use app\models\Contact;
//use app\models\Photo;

/**
 * Signup form
 */
class Search extends Model
{
    public $q;
    public $section;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['q', 'required'],
        ];
    }
    

    public function search()            
    {        
        $all = [];
        if ($this->q) {
            if ($this->section) {
                switch ($this->section) {
                    // case 'contact':
                    //     $all['contact'] = Contact::searchAnywhere($this->q);
                    //     break;
                    case 'page':
                        $all['page'] = Page::searchAnywhere($this->q);
                        break;
                    // case 'category':
                    //     $all['category'] = Category::searchAnywhere($this->q);
                    //     break;
                    case 'photo':
                        $all['photo'] = Photo::searchAnywhere($this->q);
                        break;
                    // case 'video':
                    //     $all['video'] = Video::searchAnywhere($this->q);
                    //     break;
                }
            } else {
                // all section
                //$all['video'] = Video::searchAnywhere($this->q);
                $all['page'] = Page::searchAnywhere($this->q);
                //$all['category'] = Category::searchAnywhere($this->q);
                //$all['photo'] = Photo::searchAnywhere($this->q);
                //$all['contact'] = Contact::searchAnywhere($this->q);
            }
        }        
        return $all;
    }
}
