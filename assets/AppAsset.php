<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class AppAsset
 *
 * @package app\assets
 */
class AppAsset extends AssetBundle {

    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @var string
     */
    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $css = [
        'css/font-awesome.min.css',
//        'css/bootstrap.min.css',
        'css/hover-dropdown-menu.css',
        'css/icons.css',
        'css/revolution-slider.css',
        'rs-plugin/css/settings.css',
        'css/animate.min.css',
        'css/owl/owl.carousel.css',
        'css/owl/owl.theme.css',
        'css/owl/owl.transitions.css',
        'css/prettyPhoto.css',
        'css/style.css',
        'css/responsive.css',
        'css/color.css',
    ];

    /**
     * @var array
     */
    public $js = [
//     'js/jquery.min.js',
//    'js/bootstrap.min.js',

        'js/hover-dropdown-menu.js',
        'js/jquery.hover-dropdown-menu-addon.js',
        'js/jquery.easing.1.3.js',
        'js/jquery.sticky.js',
        'js/bootstrapValidator.min.js',
        'rs-plugin/js/jquery.themepunch.tools.min.js',
        'rs-plugin/js/jquery.themepunch.revolution.min.js',
        'js/revolution-custom.js',
        'js/jquery.mixitup.min.js',
        'js/jquery.appear.js',
        'js/effect.js',
        'js/owl.carousel.min.js',
        'js/jquery.prettyPhoto.js',
        'js/jquery.parallax-1.1.3.js',
        'js/jquery.countTo.js',
        'js/tweet/carousel.js',
        'js/tweet/scripts.js',
        'js/tweet/tweetie.min.js',
        'js/jquery.mb.YTPlayer.js',
        'js/custom.js',
        'js/jquery.matchHeight-min.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}
