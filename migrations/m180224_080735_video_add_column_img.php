<?php

use yii\db\Migration;

/**
 * Class m180224_080735_video_add_column_img
 */
class m180224_080735_video_add_column_img extends Migration
{
    public function up()
    {
        $this->addColumn('{{%video}}', 'img', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('{{%video}}', 'img');
    }
}
