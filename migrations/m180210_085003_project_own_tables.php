<?php

use yii\db\Migration;

/**
 * Class m180210_085003_project_own_tables
 */
class m180210_085003_project_own_tables extends Migration {

    // Use up()/down() to run migration code without a transaction.
    public function up() {
        $this->execute("
           
            --
            -- Table structure for table `category`
            --
            
            CREATE TABLE `category` (
              `id` int(10) UNSIGNED NOT NULL,
              `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
              `category_type_id` int(10) UNSIGNED NOT NULL,
              `name_uz` varchar(255) NOT NULL,
              `name_ru` varchar(255) NOT NULL,
              `name_en` varchar(255) NOT NULL,
              `name_cyrl` varchar(255) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            -- --------------------------------------------------------

            --
            -- Table structure for table `category_type`
            --

            CREATE TABLE `category_type` (
              `id` int(10) UNSIGNED NOT NULL,
              `name` varchar(60) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            -- --------------------------------------------------------

            --
            -- Table structure for table `menu`
            --

            CREATE TABLE `menu` (
              `id` int(10) UNSIGNED NOT NULL,
              `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
              `name_uz` varchar(255) DEFAULT NULL,
              `name_ru` varchar(255) DEFAULT NULL,
              `name_en` varchar(255) DEFAULT NULL,
              `name_cyrl` varchar(255) DEFAULT NULL,
              `page_id` int(10) UNSIGNED DEFAULT NULL,
              `link` text,
              `c_order` int(10) UNSIGNED NOT NULL,
              `target_blank` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
              `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
              `visible_top` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
              `visible_side` tinyint(3) UNSIGNED NOT NULL DEFAULT '1'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            -- --------------------------------------------------------

            --
            -- Table structure for table `page`
            --
            CREATE TABLE `page` (
              `id` int(10) UNSIGNED NOT NULL,
              `category_id` int(10) UNSIGNED DEFAULT NULL,
              `name_uz` text NOT NULL,
              `name_ru` text,
              `name_en` text,
              `name_cyrl` text,
              `content_uz` longtext NOT NULL,
              `content_ru` longtext,
              `content_en` longtext,
              `content_cyrl` longtext,
              `description` text,
              `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            -- --------------------------------------------------------

            --
            -- Table structure for table `photo`
            --

            CREATE TABLE `photo` (
              `id` int(10) UNSIGNED NOT NULL,
              `group` int(10) UNSIGNED DEFAULT NULL,
              `category_id` int(10) UNSIGNED NOT NULL,
              `name_uz` varchar(255) NOT NULL,
              `name_ru` varchar(255) DEFAULT NULL,
              `name_en` varchar(255) DEFAULT NULL,
              `name_cyrl` varchar(255) DEFAULT NULL,
              `file` varchar(255) DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            -- --------------------------------------------------------

            --
            -- Table structure for table `video`
            --

            CREATE TABLE `video` (
              `id` int(10) UNSIGNED NOT NULL,
              `group` int(10) UNSIGNED DEFAULT NULL,
              `category_id` int(10) UNSIGNED NOT NULL,
              `name_uz` varchar(255) NOT NULL,
              `name_ru` varchar(255) DEFAULT NULL,
              `name_en` varchar(255) DEFAULT NULL,
              `name_cyrl` varchar(255) DEFAULT NULL,
              `file` varchar(255) DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            --
            -- Indexes for dumped tables
            --

            --
            -- Indexes for table `category`
            --
            ALTER TABLE `category`
              ADD PRIMARY KEY (`id`),
              ADD KEY `fk_category_category_type1_idx` (`category_type_id`);

            --
            -- Indexes for table `category_type`
            --
            ALTER TABLE `category_type`
              ADD PRIMARY KEY (`id`),
              ADD UNIQUE KEY `name_UNIQUE` (`name`);

            --
            -- Indexes for table `menu`
            --
            ALTER TABLE `menu`
              ADD PRIMARY KEY (`id`),
              ADD KEY `fk_menu_page1_idx` (`page_id`);

            --
            -- Indexes for table `page`
            --
            ALTER TABLE `page`
              ADD PRIMARY KEY (`id`),
              ADD KEY `fk_page_category1_idx` (`category_id`);

            --
            -- Indexes for table `photo`
            --
            ALTER TABLE `photo`
              ADD PRIMARY KEY (`id`),
              ADD KEY `fk_photo_category1_idx` (`category_id`);

            --
            -- Indexes for table `video`
            --
            ALTER TABLE `video`
              ADD PRIMARY KEY (`id`),
              ADD KEY `fk_video_category1_idx` (`category_id`);

            --
            -- AUTO_INCREMENT for dumped tables
            --

            --
            -- AUTO_INCREMENT for table `category`
            --
            ALTER TABLE `category`
              MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
            --
            -- AUTO_INCREMENT for table `menu`
            --
            ALTER TABLE `menu`
              MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
            --
            -- AUTO_INCREMENT for table `page`
            --
            ALTER TABLE `page`
              MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
            --
            -- AUTO_INCREMENT for table `photo`
            --
            ALTER TABLE `photo`
              MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
            --
            -- AUTO_INCREMENT for table `video`
            --
            ALTER TABLE `video`
              MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
            --
            -- Constraints for dumped tables
            --

            --
            -- Constraints for table `category`
            --
            ALTER TABLE `category`
              ADD CONSTRAINT `fk_category_category_type` FOREIGN KEY (`category_type_id`) REFERENCES `category_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

            --
            -- Constraints for table `menu`
            --
            ALTER TABLE `menu`
              ADD CONSTRAINT `fk_menu_page1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

            --
            -- Constraints for table `page`
            --
            ALTER TABLE `page`
              ADD CONSTRAINT `fk_page_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

            --
            -- Constraints for table `photo`
            --
            ALTER TABLE `photo`
              ADD CONSTRAINT `fk_photo_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

            --
            -- Constraints for table `video`
            --
            ALTER TABLE `video`
              ADD CONSTRAINT `fk_video_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
 
        ");
    }

    public function down() {
        echo "m180210_085003_project_own_tables cannot be reverted.\n";

        return false;
    }

}
