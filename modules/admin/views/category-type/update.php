<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryType */

$this->title = 'Kategoriya O`zgartirsh';
$this->params['breadcrumbs'][] = ['label' => 'Kategoriya turlari', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="category-type-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
