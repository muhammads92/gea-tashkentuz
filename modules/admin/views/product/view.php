<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category_id',
            'url',
            'title_uz:ntext',
            'title_ru:ntext',
            'title_en:ntext',
            'title_cyrl:ntext',
            'description_uz:ntext',
            'description_ru:ntext',
            'description_en:ntext',
            'content_uz:ntext',
            'content_ru:ntext',
            'content_en:ntext',
            'content_cyrl:ntext',
            [
                'attribute' => 'images',
                'format' => 'raw',
                'value' => Html::img(Yii::getAlias('@web').'/files/photo/products/thumb/'.$model->images, ['style'=>'width:150px; height:100px;']),
            ],
            'slider',
        ],
    ]) ?>

</div>
