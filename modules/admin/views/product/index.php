<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
             [
                'attribute' => 'category_id',
                'value' => function($model){
                    $cat = \app\models\Category::findOne($model->category_id);
                    return $cat->name_uz;
                }
            ],
            'title_uz:ntext',
            // 'title_ru:ntext',
            // 'title_en:ntext',
            //'title_cyrl:ntext',
            'description_uz:ntext',
            //'description_ru:ntext',
            //'description_en:ntext',
            'content_uz:ntext',
            //'content_ru:ntext',
            //'content_en:ntext',
            //'content_cyrl:ntext',
            //'images',
            //'c_date',
            //'viewed',
            //'slider',
            'url',

             [
                'header' => 'Rasm',
                'format' => 'raw',
                'value' => function($model){
                    return Html::img(Yii::getAlias('@web').'/files/photo/products/thumb/'.$model->images, ['style'=>'width:150px; height:100px;']);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
