<?php

use yii\helpers\Html;
use kartik\file\FileInput;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\Category::find()->all(), 'id', 'name_ru'),['prompt'=>'Категорияни танланг']) ?>

    <?= $form->field($model, 'url')->textInput() ?>
    
    <?= $form->field($model, 'title_uz')->textInput() ?>

    <?= $form->field($model, 'title_ru')->textInput() ?>

    <?= $form->field($model, 'title_en')->textInput() ?>

    <?= $form->field($model, 'title_cyrl')->textInput() ?>
    

    <?= $form->field($model, 'description_uz')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset' => 'basic'
    ]) ?>
    <?= $form->field($model, 'description_ru')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset' => 'basic'
    ]) ?>
    <?= $form->field($model, 'description_en')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset' => 'basic'
    ]) ?>
    <?= $form->field($model, 'content_uz')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset' => 'basic'
    ]) ?>
    <?= $form->field($model, 'content_ru')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset' => 'basic'
    ]) ?>
    <?= $form->field($model, 'content_en')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset' => 'basic'
    ]) ?>
    <?= $form->field($model, 'content_cyrl')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset' => 'basic'
    ]) ?>

    


    <?php
    if ($model->id && $model->images) {
        $hint = "<img src='" . Yii::getAlias('@web') . "/files/photo/products/thumb/" . $model->images . "'>";
    } else {
        $hint = '';
    }
    ?>
    <?php
    echo $form->field($model, 'images', ['template' => "{label}\n{hint}\n{input}\n{error}"])
            ->widget(\kartik\file\FileInput::className())
            ->hint($hint, ['style' => 'margin-bottom:2px;']);
    ?>




    <?= $form->field($model, 'slider')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
