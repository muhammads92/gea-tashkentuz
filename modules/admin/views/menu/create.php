<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Menu */

$this->title = 'Menyu qo\'shish';
$this->params['breadcrumbs'][] = ['label' => 'Menyu', 'url' => ['index']];
$tbreadcrumbs= [];
$tmodel = $model->parent;
while($tmodel){
    $tbreadcrumbs[] = ['label' => $tmodel->name_uz, 'url' => ['view', 'id'=>$tmodel->id]];
    $tmodel = $tmodel->parent;
}
for($i=count($tbreadcrumbs)-1;$i>=0;$i--){
    $this->params['breadcrumbs'][] = $tbreadcrumbs[$i];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
