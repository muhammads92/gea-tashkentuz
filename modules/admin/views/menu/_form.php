<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>

    <?php //$form->field($model, 'name_cyrl')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'page_id')->widget(\kartik\select2\Select2::className(), [
        'data' => app\models\Page::all(),
        'pluginOptions' => [
            'allowClear' => TRUE,
        ],
        'options' => [
            'placeholder' => ''
        ]
    ]) ?>

    <?= $form->field($model, 'link')->textInput() ?>

    <?= $form->field($model, 'c_order')->textInput() ?>

    <?= $form->field($model, 'target_blank')->checkbox() ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <?= $form->field($model, 'visible_top')->checkbox() ?>

    <?= $form->field($model, 'visible_side')->checkbox() ?>

    <?= $form->field($model, 'parent_id')->dropDownList([NULL=>NULL]+\app\models\Menu::all($model->id)) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Qo`shish' : 'O\'zgartirish', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
