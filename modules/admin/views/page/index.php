<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sahifa';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="page-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Sahifa Qo`shish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'category_id',
            'name_uz:ntext',
            'name_ru:ntext',
            'name_en:ntext',
            //'name_cyrl:ntext',
            //'content_uz:ntext',
            //'content_ru:ntext',
            //'content_en:ntext',
            //'content_cyrl:ntext',
            //'description:ntext',
            //'created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
