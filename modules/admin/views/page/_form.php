<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'category_id')->dropDownList(app\models\Category::all()) ?>

    
    <?= $form->field($model, 'name_uz')->textInput() ?>

    <?= $form->field($model, 'name_ru')->textInput() ?>

    <?= $form->field($model, 'name_en')->textInput() ?>

    <?= $form->field($model, 'name_cyrl')->textInput() ?>

    <?= $form->field($model, 'content_cyrl')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    

    <?= $form->field($model, 'content_uz')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
        ]) ?>
    <?= $form->field($model, 'content_ru')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
        ]) ?>
    <?= $form->field($model, 'content_en')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
        ]) ?>

    <?//= $form->field($model, 'created')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
