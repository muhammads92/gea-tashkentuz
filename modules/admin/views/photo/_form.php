<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Photo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photo-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php // $form->field($model, 'group')->textInput() ?>
    <?= $form->field($model, 'category_id')->dropDownList(app\models\Category::all('photo')) ?>


    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?php //$form->field($model, 'name_cyrl')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chosed')->dropDownList([0=> "Yoq", 1=> "Xa"])?>
    <?php
    if ($model->id && $model->file) {
        $hint = "<img src='" . Yii::getAlias('@web') . "/files/photo/thumb/" . $model->file . "'>";
    } else {
        $hint = '';
    }
    ?>
    <?php
    echo $form->field($model, 'fileFile', ['template' => "{label}\n{hint}\n{input}\n{error}"])
            ->widget(\kartik\file\FileInput::className())
            ->hint($hint, ['style' => 'margin-bottom:2px;']);
    ?>

    <div style="padding-top: 20px;">

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Qo\'shish' : 'O\'zgartirish', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
