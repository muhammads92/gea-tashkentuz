<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\PhotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rasmlar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Qo\'shish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'category_id',
                'value' => 'category.name_uz',
                'filter' => \app\models\Category::all('photo'),
            ],
            'name_uz',
            'name_ru',
            'name_en',
            // 'name_cyrl',
            // 'file',
            [
                'attribute' => 'chosed',
                'value' => function($model){
                    return ($model->chosed == 0)?'Yoq':'Xa';
                }
            ],
            [
                'header' => 'Rasm',
                'format' => 'raw',
                'value' => function($model){
                    return Html::img(Yii::getAlias('@web').'/files/photo/thumb/'.$model->file, ['style'=>'width:150px; height:100px;']);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
