<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Photo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rasmlar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-view">

   

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            [
                'attribute' => 'category_id',
                'value' => $model->category->name_uz,
            ],
            'name_uz',
            'name_ru',
            'name_en',
            //'name_cyrl',
            [
                'attribute' => 'chosed',
                'value' => ($model->chosed == 0)?'Yoq':'Xa'
            ],
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => Html::img(Yii::getAlias('@web').'/files/photo/thumb/'.$model->file, ['style'=>'width:150px; height:100px;']),
            ],
        ],
    ]) ?>

</div>
