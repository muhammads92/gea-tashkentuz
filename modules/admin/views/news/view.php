<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Yangiliklar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">



    <p>
        <?= Html::a('O`zgarttish', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category_id',
            'title_uz:ntext',
            'title_ru:ntext',
            'title_en:ntext',
            'title_cyrl:ntext',
            'description_uz:ntext',
            'description_ru:ntext',
            'description_en:ntext',
            'description_cyrl:ntext',
            'content_uz:ntext',
            'content_ru:ntext',
            'content_en:ntext',
            'content_cyrl:ntext',
            'images',
            'slider',
        ],
    ]) ?>

</div>
