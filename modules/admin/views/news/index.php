<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Yangiliklar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Yangiliklar qo`shish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'category_id',
            'title_uz:ntext',
            // 'title_ru:ntext',
            // 'title_en:ntext',
            //'title_cyrl:ntext',
            'description_uz:ntext',
            //'description_ru:ntext',
            //'description_en:ntext',
            //'description_cyrl:ntext',
            'content_uz:ntext',
            //'content_ru:ntext',
            //'content_en:ntext',
            //'content_cyrl:ntext',
            'images',
            //'c_date',
            //'viewed',
            //'slider',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
