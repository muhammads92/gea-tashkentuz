<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Product;
use app\modules\admin\models\search\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\imagine\Image;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $file = \yii\web\UploadedFile::getInstance($model, 'images');
            $path = Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR;
            
            if (!empty($file->name)) {
                
                $fileName = $model->id . "." . $file->extension;
                $file->saveAs($path . $fileName);
                
                Image::thumbnail(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR  . $fileName, 120, 120)
                    ->save(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR. 'thumb' . DIRECTORY_SEPARATOR. $fileName, ['quality'=> 80]);
                
                Image::thumbnail(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR  . $fileName, 300, 200)
                    ->save(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR. 'bigthumb' . DIRECTORY_SEPARATOR. $fileName, ['quality'=> 80]);
                
                $model->images = $fileName;
                $model->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }





    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
         $model = $this->findModel($id);
        $oldfilename = $model->images;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $file = \yii\web\UploadedFile::getInstance($model, 'images');
            $path = Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR;
            if (!empty($file->name)) {

                $fileName = $model->id . "." . $file->extension;
                $file->saveAs($path . $fileName);
                
                Image::thumbnail(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR  . $fileName, 120, 120)
                    ->save(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR. 'thumb' . DIRECTORY_SEPARATOR. $fileName, ['quality'=> 80]);
                Image::thumbnail(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR  . $fileName, 300, 200)
                    ->save(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR. 'bigthumb' . DIRECTORY_SEPARATOR. $fileName, ['quality'=> 100]);

                $model->images = $fileName;
                $model->save();

                if ($oldfilename != $fileName && $oldfilename) {
                    @unlink($path . $oldfilename);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
