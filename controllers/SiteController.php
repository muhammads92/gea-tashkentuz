<?php

namespace app\controllers;

use app\models\forms\ContactForm;
use app\models\forms\ResetPasswordForm;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii2mod\rbac\filters\AccessControl;
use app\models\Contact;
use app\models\About;
use app\models\Photo;
use app\models\Product;
use app\models\Category;
use yii\data\Pagination;



/**
 * Class SiteController
 *
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [
                    'login',
                    'logout',
                    'signup',
                    'request-password-reset',
                    'password-reset',
                    'account',
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup', 'request-password-reset', 'password-reset'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'account'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['get'],
                    'contact' => ['get', 'post'],
                    'account' => ['get', 'post'],
                    'login' => ['get', 'post'],
                    'logout' => ['post'],
                    'signup' => ['get', 'post'],
                    'request-password-reset' => ['get', 'post'],
                    'password-reset' => ['get', 'post'],
                    'page' => ['get', 'post'],
                ],
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'login' => [
                'class' => 'yii2mod\user\actions\LoginAction',
            ],
            'logout' => [
                'class' => 'yii2mod\user\actions\LogoutAction',
            ],
            'signup' => [
                'class' => 'yii2mod\user\actions\SignupAction',
            ],
            'request-password-reset' => [
                'class' => 'yii2mod\user\actions\RequestPasswordResetAction',
            ],
            'password-reset' => [
                'class' => 'yii2mod\user\actions\PasswordResetAction',
            ],
            'page' => [
                'class' => 'yii2mod\cms\actions\PageAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $model = new Contact();

        return $this->render('index',['model=>$model']);
    }

    /**
     * Displays contact page.
     *
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Contact();

        if ($model->load(Yii::$app->request->post())) {
           $model->created = date('Y-m-d h:m:s');
           $model->save();
           return $this->redirect(['index', 'id' => $model->id]);
       } else {
        return $this->render('index', [
            'model' => $model,
            ]);
    }
}
public function actionUpdate($id)
{
    $model = $this->findModel($id);
    $cats = Category::find()->all();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
        return $this->redirect(['view', 'id' => $model->id]);
    } else {
        return $this->render('update', [
            'model' => $model,
            'cats' => $cats, ]);
    }
}
public function actionContact()
{
    $model = new ContactForm();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        $model->created = date('Y-m-d h:m:s');
        $model->save();

        Yii::$app->session->setFlash('success', Yii::t('app', 'Thank you for contacting us. We will respond to you as soon as possible.'));

        return $this->refresh();



    }else {
        Yii::$app->session->setFlash('error', Yii::t('app', 'There was an error sending email.'));
    }


    return $this->render('contact', [
        'model' => $model,
        ]);

}

    /**
     * Displays account page.
     *
     * @return string|\yii\web\Response
     */
    public function actionAccount()
    {
        $resetPasswordForm = new ResetPasswordForm(Yii::$app->user->identity);

        if ($resetPasswordForm->load(Yii::$app->request->post()) && $resetPasswordForm->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Password has been updated.'));

            return $this->refresh();
        }

        return $this->render('account', [
            'resetPasswordForm' => $resetPasswordForm,
            ]);
    }

    public function actionSearch($q = NULL)
    {
       // $this->layout = 'column2';
        if($q){
            $model = new \app\models\Search();
            $model->q = $q;
            $result = $model->search();

        }else{
            $result = [];
        }
        return $this->render('search', [
            'result' => $result
            ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionProfnastil()
    {

       return $this->render('profnastil');

   }

    public function actionProduct()
    {
        // $model = Product::findOne($id);
        return $this->render('product', [
            // 'model' => $model,
        ]);

    }


   public function actionGalereya()
   {

       $model = new Category();
       $categories = Category::find()->all();
       $images = Photo::find()->orderBy('category_id')->all();
       return $this->render('galereya', [
           'model' => $model,
           'categories' => $categories,
           'images' => $images
           ]);


        // return $this->render('galereya');

   }

}
